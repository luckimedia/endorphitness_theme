<?php
/**
 * Template Name: Dashboard Overview Template
 */
wc_clear_notices();
?>
<!-- App Header -->
<?php include('subscription-src/includes/global-app-header.php'); ?>
<!-- End App header -->

<main class="dashboard-main">
    <section class="section intro">
        <?php include('subscription-src/includes/global-app-header-partial.php'); ?>

        <!-- Middle Section -->
        <div class="panels-parent">

            <div class="container">
                <div class="content-box">
                    <h1 class="text-green">Hi there, <span
                            class="text-purple"><?php echo $current_user->user_firstname; ?></span></h1>
                    <?php if( have_rows('dashboard-top-area', 'option') ):
                    while ( have_rows('dashboard-top-area', 'option') ) : the_row(); ?>
                    <p><?php the_sub_field('paragraph-after-heading'); ?></p>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="container workout-cards pt-3">
                <div class="content-box"></div>
                <?php if( have_rows('workouts-card', 'option') ): while ( have_rows('workouts-card', 'option') ) : the_row(); ?>
                <div class="ui-card bg-gray clickable" onclick="location.href='<?php the_sub_field('url'); ?>'">
                    <div class="top-content">
                        <figure>
                            <img src="<?php the_sub_field('image'); ?>" alt="Client Image">
                        </figure>
                    </div>
                    <div class="bottom-content">
                        <h2 class="text-purple"><?php the_sub_field('title'); ?></h2>
                        <?php $trimmed_description = get_sub_field('description'); ?>
                        <p><?php echo limit_text($trimmed_description, 70); ?></p>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="container other-cards pt-4 pt-lg-3">
                <div class="row">
                    <div class="col-md-12 col-lg-12 mb-4 mb-lg-4">
                        <?php if( have_rows('week-overview', 'option') ):
                        while ( have_rows('week-overview', 'option') ) : the_row(); ?>
                        <div class="ui-card bg-gray content-card week-overview">
                            <div class="bottom-content text-left text-lg-center remove-constraint">
                                <h2 class="text-purple"><?php the_sub_field('heading'); ?></h2>
                                <hr class="bg-purple ml-lg-auto mr-lg-auto" />
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-6 col-lg-4 mb-4 mb-lg-5">
                        <?php if( have_rows('recipes-card', 'option') ):
                            while ( have_rows('recipes-card', 'option') ) : the_row(); ?>
                        <div class="ui-card bg-gray clickable" onclick="location.href='<?php the_sub_field('url'); ?>'">
                            <div class="top-content">
                                <figure>
                                    <img src="<?php the_sub_field('image'); ?>" alt="Client Image">
                                </figure>
                            </div>
                            <div class="bottom-content">
                                <h2 class="text-purple"><?php the_sub_field('title'); ?></h2>
                                <?php $trimmed_description = get_sub_field('description'); ?>
                                <p><?php echo limit_text($trimmed_description, 35); ?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-6 col-lg-4 mb-4 mb-lg-5">
                        <?php if( have_rows('forums-card', 'option') ):
                        while ( have_rows('forums-card', 'option') ) : the_row(); ?>
                        <div class="ui-card bg-gray clickable" onclick="location.href='<?php the_sub_field('url'); ?>'">
                            <div class="top-content">
                                <figure>
                                    <img src="<?php the_sub_field('image'); ?>" alt="Client Image">
                                </figure>
                            </div>
                            <div class="bottom-content">
                                <h2 class="text-purple"><?php the_sub_field('title'); ?></h2>
                                <?php $trimmed_description = get_sub_field('description'); ?>
                                <p><?php echo limit_text($trimmed_description, 35); ?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-12 col-lg-4 mb-4 mb-lg-5">
                        <?php if( have_rows('stretching-card', 'option') ):
                        while ( have_rows('stretching-card', 'option') ) : the_row(); ?>
                        <div class="ui-card bg-gray clickable" onclick="location.href='<?php the_sub_field('url'); ?>'">
                            <div class="top-content">
                                <figure>
                                    <img src="<?php the_sub_field('image'); ?>" alt="Client Image">
                                </figure>
                            </div>
                            <div class="bottom-content">
                                <h2 class="text-purple"><?php the_sub_field('title'); ?></h2>
                                <?php $trimmed_description = get_sub_field('description'); ?>
                                <p><?php echo limit_text($trimmed_description, 35); ?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-6 col-lg-6 mb-4 mb-lg-0">
                        <?php if( have_rows('content-card', 'option') ):
                        while ( have_rows('content-card', 'option') ) : the_row(); ?>
                        <div class="ui-card bg-gray content-card">
                            <div class="bottom-content">
                                <h2 class="text-purple"><?php the_sub_field('heading'); ?></h2>
                                <hr class="bg-purple" />
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-6 col-lg-6 mb-4 mb-lg-0">
                        <?php if( have_rows('content-card-two', 'option') ):
                        while ( have_rows('content-card-two', 'option') ) : the_row(); ?>
                        <div class="ui-card bg-gray content-card">
                            <div class="bottom-content">
                                <h2 class="text-purple"><?php the_sub_field('heading'); ?></h2>
                                <hr class="bg-purple" />
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                </div>
            </div>

        </div>
        <!-- End Middle Section -->

        <div class="app-footer hide-below-1200">
            <div class="container text-center">
                <h6>Copyright <?php echo date('Y'); ?></h6>
            </div>
        </div>
    </section>
</main>

<!-- App Footer -->
<?php include('subscription-src/includes/global-app-footer.php'); ?>
<!-- End App Footer -->