<?php

get_template_part('templates/particals', 'header');

if (is_home() || is_archive()) :
    get_template_part('templates/archive', $post->post_type );

elseif (is_shop()) :
    get_template_part('templates/archive', $post->post_type = 'shop' );

elseif (is_checkout()) :
    get_template_part('templates/archive', $post->post_type = 'checkout' );

elseif (is_cart()) :
    get_template_part('templates/archive', $post->post_type = 'cart' );

elseif (is_product()) :
    get_template_part('woocommerce/content-single-product' );

elseif (is_account_page()) :
    get_template_part('templates/archive', $post->post_type = 'my-account' );

else :
    get_template_part('templates/single', $post->post_type );
endif;

get_template_part('templates/particals', 'footer');