<?php
    /**
     * workout Custom Post Type
     *
     * @workoutCPT
     * @package designpickle
     * @category Class
     * @author Ben Leeth
     */

    if(!defined('ABSPATH')) {
        header('HTTP/1.0 403 Forbidden');
        exit;
    }

    class GroceryCPT {

        public function __construct() {
            $cpt_labels = [
                'name' => __('Groceries'),
                'menu_name' => __('Groceries'),
                'all_items' => __('Groceries')
            ];
            $cpt_args = [
                'menu_icon' => 'dashicons-list-view',
                'supports' => ['title', 'page-attributes', 'custom-fields','editor', 'thumbnail', 'revisions'],
                'public' => true,
                'hierarchical' => true,
                'has_archive' => true,
                'rewrite' => [
                    'slug'       => 'grocery',
                    'with_front' => false,
                ],
            ];
            $cpt = new CustomPostType('grocery', $cpt_labels, $cpt_args);

            $cpt->register_routes();
        }

    }

    $logos_cpt = new GroceryCPT();