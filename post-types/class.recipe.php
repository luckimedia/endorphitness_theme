<?php
    /**
     * Recipe Custom Post Type
     *
     * @RecipeCPT
     * @package designpickle
     * @category Class
     * @author Ben Leeth
     */

    if(!defined('ABSPATH')) {
        header('HTTP/1.0 403 Forbidden');
        exit;
    }

    class RecipesCPT {

        public function __construct() {
            $cpt_labels = [
                'name' => __('Recipes'),
                'menu_name' => __('Recipes'),
                'all_items' => __('Recipes'),
                'add_new_item' => __('Add New Recipe'),
            ];
            $cpt_args = [
                'menu_icon' => 'dashicons-welcome-write-blog',
                'supports' => ['title', 'page-attributes', 'editor', 'thumbnail', 'revisions'],
                'public' => true,
                'hierarchical' => true,
                'has_archive' => true,
                'rewrite' => [
                    'slug'       => 'recipes',
                    'with_front' => false,
                ],
            ];
            $cpt = new CustomPostType('recipes', $cpt_labels, $cpt_args);
            $tax_1_args = [
                'hierarchical' => true,
                'show_admin_column' => true,
                'label' => 'Categories',
                'singular_label' => 'Category',
                'rewrite' => [
                    'slug'       => 'categories',
                    'with_front' => false,
                ],
            ];
            $cpt->add_taxonomy('recipes-categories', array('recipes'), $tax_1_args);
            $cpt->register_routes();
        }

    }

    $logos_cpt = new RecipesCPT();
