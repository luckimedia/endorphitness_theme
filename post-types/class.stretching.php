<?php
/**
 * Stretching Custom Post Type
 *
 * @StretchingCPT
 * @package designpickle
 * @category Class
 * @author Ben Leeth
 */

if(!defined('ABSPATH')) {
    header('HTTP/1.0 403 Forbidden');
    exit;
}

class StretchingCPT {

    public function __construct() {
        $cpt_labels = [
            'name' => __('Stretching & Mobility'),
            'menu_name' => __('Stretching & Mobility'),
            'all_items' => __('Stretching & Mobility'),
            'add_new_item' => __('Add New Stretching'),
        ];
        $cpt_args = [
            'menu_icon' => 'dashicons-lightbulb',
            'supports' => ['title', 'page-attributes', 'editor', 'thumbnail', 'revisions'],
            'public' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'rewrite' => [
                'slug'       => 'stretching-mobility',
                'with_front' => false,
            ],
        ];
        $cpt = new CustomPostType('Stretching', $cpt_labels, $cpt_args);
        $tax_1_args = [
            'hierarchical' => true,
            'show_admin_column' => true,
            'label' => 'Categories',
            'singular_label' => 'Category',
            'rewrite' => [
                'slug'       => 'stretching-categories',
                'with_front' => false,
            ],
        ];
        $cpt->add_taxonomy('stretching-categories', array('stretching'), $tax_1_args);
        $cpt->register_routes();
    }

}

$logos_cpt = new StretchingCPT();
