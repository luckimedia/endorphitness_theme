<?php
    /**
     * workout Custom Post Type
     *
     * @workoutCPT
     * @package designpickle
     * @category Class
     * @author Ben Leeth
     */

    if(!defined('ABSPATH')) {
        header('HTTP/1.0 403 Forbidden');
        exit;
    }

    class WorkoutCPT {

        public function __construct() {
            $cpt_labels = [
                'name' => __('Workouts'),
                'menu_name' => __('Workouts'),
                'all_items' => __('Workouts')
            ];
            $cpt_args = [
                'menu_icon' => 'dashicons-list-view',
                'supports' => ['title', 'page-attributes', 'custom-fields','editor', 'thumbnail', 'revisions'],
                'public' => true,
                'hierarchical' => true,
                'has_archive' => true,
                'rewrite' => [
                    'slug'       => 'workouts',
                    'with_front' => false,
                ],
            ];
            $cpt = new CustomPostType('workout', $cpt_labels, $cpt_args);

            $cpt->register_routes();
        }

    }

    $logos_cpt = new WorkoutCPT();
