<?php
/**
 * Testimonials Custom Post Type
 *
 * @TestimonialsCPT
 * @package designpickle
 * @category Class
 * @author Ben Leeth
 */

if(!defined('ABSPATH')) {
	header('HTTP/1.0 403 Forbidden');
	exit;
}

class TestimonialsCPT {

	public function __construct() {
		$cpt_labels = [];
		$cpt_args = [
			'menu_icon' => 'dashicons-format-quote',
			'supports' => ['title', 'thumbnail']
		];
		$cpt = new CustomPostType('Testimonial', $cpt_labels, $cpt_args);
		$cpt->add_meta_box(
			'Testimonial Info',
			[
				[
					'label' => 'User Name',
					'width' => 100,
					'type' => 'text',
					'placeholder' => 'UserName...'
				], [
					'label' => 'User Location',
					'width' => 100,
					'type' => 'text',
					'placeholder' => 'Position...'
				], [
					'label' => 'Quote',
					'width' => 100,
					'type' => 'richtext'
				], [
					'label' => 'Rating1',
					'name' => 'rating_select',
					'type' => 'select',
					'options' => array (
						'java'       => 'Java',
						'javascript' => 'JavaScript',
						'php'        => 'PHP',
						'csharp'     => 'C#',
						'objectivec' => 'Objective-C',
						'kotlin'     => 'Kotlin',
						'swift'      => 'Swift',
					)
				]
			]
		);
		$cpt->register_routes();
	}
}

$testimonials_cpt = new TestimonialsCPT();
