<?php
/**
 * Set Constants
 */
define('THEME_URL', get_template_directory_uri());

/**
 * Load Custom Files
 */
include_once __DIR__ . '/includes/theme-support.php';
include_once __DIR__ . '/includes/custom-functions.php';
include_once __DIR__ . '/includes/custom-post-types.php';
include_once __DIR__ . '/includes/class.wp-bootstrap-walker.php';
include_once __DIR__ . '/includes/enqueue.php';
include_once __DIR__ . '/includes/menus.php';
include_once __DIR__ . '/includes/customizer.php';
include_once __DIR__ . '/includes/acf-options-page.php';
include_once __DIR__ . '/includes/custom-subscription-functions.php';
/**
 * Add ACF Fields
 */
require_once __DIR__ . '/acf/init.php';