<?php
    get_template_part('templates/particals', 'header');
    get_template_part('templates/blog', 'category_hero_module');
    get_template_part('templates/blog', 'filter_topic_module');
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
?>
<section class="blog-box-module<?php if( $wp_query->current_post%2 == 1 ){ echo ' blog-box-module--right';}else{ echo '';} ?>">
    <div class="container">
        <div class="row  no-gutters align-items-center">
            <div class="col-lg-5 blog-box-module__img">
                <div class="blog-box-module__image">
                    <img src="<?php echo $featured_img_url; ?>" class="img-fluid" style="width: 100%;">
                </div>
            </div>
            <div class="col-lg-7 blog-box-module__desc">
                <div class="blog-box-module__info">
                    <div class="bio">
                        <div class="bio__image">
                            <img src="<?php echo THEME_URL; ?>/images/t001.jpg" class="img-fluid">
                        </div>
                        <div class="bio__content">
                            <span class="bio__title"><?php echo get_the_author(); ?></span>
                            <span class="bio__date"><?php echo get_the_date(); ?></span>
                        </div>
                    </div>
                   <a href="<?php the_permalink(); ?>"> <h4 class="blog-box-module__title"><?php the_title(); ?> <?php echo '['.date("Y", strtotime(get_the_date())).']'; ?></h4></a>
                    <p><?php the_excerpt(); ?></p>
                    <a href="<?php the_permalink(); ?>" class="btn btn--nevyblue">Read More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    endwhile;
        if(function_exists('wp_paginate')):
                wp_paginate();  
        endif;
	endif;
?>
<?php
    get_template_part('templates/blog', 'posts_module');
    get_template_part('templates/blog', 'newsletter_module');
    get_template_part('templates/blog', 'category_module');
    get_template_part('templates/particals', 'footer');
?>