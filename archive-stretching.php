<?php
/**
 * Template Name: Archive Stretching
 *  Template Post Type: Stretching
 */
?>
<!-- App Header -->
<?php include('subscription-src/includes/global-app-header.php'); ?>
<!-- End App header -->

<main class="dashboard-main workouts-overview stretching-overview">
    <section class="section intro">
        <?php include('subscription-src/includes/global-app-header-partial.php'); ?>

        <!-- Middle Section -->
        <div class="panels-parent">
            <?php 
                global $userdata,$user_identity;
                get_currentuserinfo();
            ?>
                <div class="container">
                    <?php if( have_rows('stretching-top-area', 'option') ):
                        while ( have_rows('stretching-top-area', 'option') ) : the_row(); ?>
                        <div class="content-box">
                            <h1 class="text-green"><?php the_sub_field('heading'); ?></h1>
                            <p><?php the_sub_field('description'); ?></p>
                        </div>
                        <?php endwhile; ?>
                    <?php endif; ?> 
                </div>
                <!-- Loop Here -->
                    <?php 
                    $custom_terms = get_terms('stretching-stretching-categories', 'orderby=name&order=DESC');

                    foreach($custom_terms as $custom_term) {
                        wp_reset_query();
                        $args = array('post_type' => 'stretching',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'stretching-stretching-categories',
                                    'field' => 'slug',
                                    'terms' => $custom_term->slug,
                                ),
                            ),
                            );

                            $loop = new WP_Query($args);
                            if($loop->have_posts()) { ?>
                            <!--  -->
                            <div class="container other-cards pt-3 pt-lg-4">
                                <div class="category-title-wrapper pb-4">
                                        <h2 class="text-purple"><?php echo $custom_term->name; ?></h2>
                                </div>
                                <div class="row">   
                            <?php while($loop->have_posts()) : $loop->the_post(); ?>
                                <div class="col-md-4 mb-5">
                                    <div class="ui-card bg-gray clickable" onclick="location.href='<?= get_permalink(get_the_ID()); ?>'">
                                        <div class="top-content">
                                            <figure>
                                                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>
                                                <img src="<?= $featured_img_url ?>" alt="Workout Image">
                                            </figure>
                                        </div>
                                        <div class="bottom-content">
                                            <h2 class="text-purple"><?= get_the_title(); ?></h2>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;
                            } ?>
                            </div>
                        </div>
                    <?php }

                    ?>
                    <!-- End loop -->
        </div>
        <!-- End Middle Section -->

        <div class="app-footer hide-below-1200">
            <div class="container text-center">
                <h6>Copyright <?php echo date('Y'); ?></h6>
            </div>
        </div>
    </section>
</main>

<!-- App Footer -->
<?php include('subscription-src/includes/global-app-footer.php'); ?>
<!-- End App Footer -->
