<?php
/**
 * Template Name: Single Workouts
 *  Template Post Type: workout
 */
?>
<!-- App Header -->
<?php include('subscription-src/includes/global-app-header.php'); ?>
<!-- End App header -->

<main class="dashboard-main workouts-overview workouts-single">
    <section class="section intro">
        <?php include('subscription-src/includes/global-app-header-partial.php'); ?>

        <!-- Middle Section -->
        <div class="panels-parent">
            <div class="container">
                <?php 
                        if ( have_posts() ) {
                            while ( have_posts() ) {
                                the_post(); ?>
                <div class="single-wrapper">
                    <a class="hide-below-1200" href="<?php echo home_url('workouts'); ?>">
                        <div class="back-arrow">
                            <i class="fas fa-arrow-circle-left"></i>
                        </div>
                    </a>
                    <div class="xmlsma--sx">
                        <div class="skuq-ml-j0xb title-wrapper text-center">
                            <h1 class="text-purple"><?php the_title(); ?></h1>
                            <?php if( get_field('single-workouts-title') ): ?>
                            <h2 class=""><?php the_field('single-workouts-title'); ?></h2>
                            <?php endif; ?>
                            <!--<p class="text-green date"><?php echo get_the_date(); ?></p> -->
                        </div>

                        <!-- Image or Video -->

                        <?php if( get_field('video_iframe') ): ?>
                        <div class="skuq-ml-j0xb featured-image-wrapper featured-video-wrapper">
                            <?php the_field('video_iframe'); ?>
                        </div>
                        <?php 
                            preg_match( '@src="([^"]+)"@' , get_field('video_iframe'), $match );
                            $youtube_video_src = array_pop($match);
                            preg_match("#([\/|\?|&]vi?[\/|=]|youtu\.be\/|embed\/)([a-zA-Z0-9_-]+)#", $youtube_video_src, $match);
                            $youtube_video_src = end($match);
                        ?>
                        <div class="text-right">
                            <a target="_blank" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_src; ?>">
                                <button type="button" class="btn primary-button"
                                    style="font-size: 0.9rem; border-radius: 4px; padding: 0.57rem 1rem;">Watch
                                    on YouTube</button></a>
                        </div>
                        <?php endif; ?>

                        <?php if (has_post_thumbnail( $post->ID ) && !get_field('video_iframe')): ?>
                        <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                        <div class="skuq-ml-j0xb featured-image-wrapper">
                            <figure>
                                <img src="<?php echo $image[0]; ?>" alt="" />
                            </figure>
                        </div>
                        <?php endif; ?>

                        <!-- //Image or Video -->

                        <div class="skuq-ml-j0xb content-wrapper">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?php } // end while
                        } // end if
                    ?>
            </div>
        </div>
        <!-- End Middle Section -->

        <div class="app-footer hide-below-1200">
            <div class="container text-center">
                <h6>Copyright <?php echo date('Y'); ?></h6>
            </div>
        </div>
    </section>
</main>
<!-- App Footer -->
<?php include('subscription-src/includes/global-app-footer.php'); ?>
<!-- End App Footer -->