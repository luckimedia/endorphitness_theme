jQuery(document).ready(function () {
  // Header
  const menuToggle = document.querySelector(".js-toggle-menu");
  const header = document.querySelector(".header-wrapper");
  menuToggle.addEventListener("click", function () {
    menuToggle.classList.toggle("is-active");
    header.classList.toggle("drawer-open");
    jQuery("body, html").toggleClass("scroll-lock");
  });
  let clicked = false;

  (function (jQuery) {
    jQuery.fn.clickToggle = function (func1, func2) {
      var funcs = [func1, func2];
      this.data("toggleclicked", 0);
      this.click(function () {
        var data = jQuery(this).data();
        var tc = data.toggleclicked;
        jQuery.proxy(funcs[tc], this)();
        data.toggleclicked = (tc + 1) % 2;
      });
      return this;
    };
  })(jQuery);

  jQuery("#nav-drawer .has-sub-navigation")
    .click(function (e) {
      if (e.clientX > jQuery(this).offset().left + 160) {
        // jQuery(this).addClass('show-my-children');
        if (!clicked && !jQuery(this).hasClass("show-my-children")) {
          jQuery(this).addClass("show-my-children");
          clicked = true;
        } else {
          jQuery(this).removeClass("show-my-children");
          clicked = false;
        }
        jQuery(this).children("ul").slideToggle("fast");
      }
      //select all the `.child` elements and stop the propagation of click events on the elements
    })
    .children("ul")
    .click(function (event) {
      event.stopPropagation();
    });

  jQuery("header.dashboard-header .header-wrapper .drawer .drawer-inner ul li a").click(function (e) {
    // Do something
    e.stopPropagation();
  });

  // jQuery('#nav-drawer li.has-sub-navigation').clickToggle(function(e) {
  //      if (e.clientX > jQuery(this).offset().left + 250) {
  //     jQuery(this).addClass('show-my-children');
  //   }
  // },
  // function() {
  //     jQuery(this).removeClass('show-my-children');
  // });

  jQuery("#app-header-toggler").clickToggle(
    function () {
      jQuery(".dashboard-header").addClass("lose-width");
      jQuery(".dashboard-header #nav-drawer").addClass("lose-width");
    },
    function () {
      jQuery(".dashboard-header").removeClass("lose-width");
      jQuery(".dashboard-header #nav-drawer").removeClass("lose-width");
    }
  );

  const messageOne = jQuery(".woocommerce-form-coupon-toggle");
  const messageTwo = jQuery("form.checkout_coupon.woocommerce-form-coupon");
  jQuery(".col-lg-4 .content-box").append(messageOne);
  jQuery(".col-lg-4 .content-box").append(messageTwo);

  jQuery(function () {
    pageUrl = window.location.href;
    jQuery("header.dashboard-header .header-wrapper .drawer .drawer-inner ul li a").each(function () {
      link = jQuery(this);
      if (link.attr("href") == pageUrl) {
        link.addClass("active");
      }
    });
  });

  tippy("[data-tippy-content]");

  // jQuery(".js-modal-btn").modalVideo();
});
