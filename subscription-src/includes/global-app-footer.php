<div class="app-footer mobile--footer">
    <div class="container text-center">
        <h6>Copyright <?php echo date('Y'); ?></h6>
    </div>
</div>
</div>
</div>

<?php wp_footer(); ?>
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script src="<?php echo THEME_URL; ?>/subscription-src/main.js"></script>
<!-- //Javascript Files End -->
</body>

</html>