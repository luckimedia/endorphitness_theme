<div class="app-header hide-below-1200">
    <div class="current-plan" data-tippy-content="Click to change plan">
        <a href="<?php echo home_url($collective_module_url); ?>">
            <?php if( current_user_can('editor') || current_user_can('administrator') ) {  ?>
            Site Admin
            <?php } else { ?>
            <?php echo get_active_plan_name(); ?>
            <?php } ?>
        </a>
    </div>
    <div class="container">
        <div class="app-header-flex">
            <div class="child">
                <div id="app-header-toggler" class="hamburger-parent d-wrapper">
                    <button role="button" class="d-btn site-menu-toggle js-toggle-menu">
                        <span class="hamburger-line hamburger-line--top"></span>
                        <span class="hamburger-line hamburger-line--middle"></span>
                        <span class="hamburger-line hamburger-line--bottom"></span>
                    </button>
                </div>
            </div>
            <div class="child custom-permalinks">
                <ul>
                    <li><a href="<?php echo home_url('my-account'); ?>">Manage Account</a></li>
                    <li><a href="<?php echo home_url('my-account/customer-logout'); ?>">Log Out</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>