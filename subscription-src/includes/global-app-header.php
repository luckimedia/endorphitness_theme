<?php 
    if( has_active_subscription() || current_user_can( 'manage_options' ) ){
    } else if ((!has_active_subscription() || !current_user_can( 'manage_options' )) && is_user_logged_in() ) {
        wp_redirect(get_permalink( wc_get_page_id( 'myaccount' ) ));
        exit();
    } else {
        wp_redirect(get_permalink( wc_get_page_id( 'myaccount' ) ));
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>
            <?php bloginfo('name'); if(wp_title('', false)) { echo ' | '; } else { echo bloginfo('description'); } wp_title(''); ?>
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel='icon' href='<?php echo THEME_URL; ?>/images/E-Logo-Green.png' type='image/x-icon' />
        <link rel="stylesheet" href="<?php echo THEME_URL; ?>/css/site.css">
        <link rel="stylesheet" href="<?php echo THEME_URL; ?>/woocommerce/assets/css/woocommerce.css">
        <link rel="stylesheet" href="<?php echo THEME_URL; ?>/subscription-src/bootstrap-grids.css">
        <link rel="stylesheet" href="<?php echo THEME_URL; ?>/fonts/fonts.css">
        <!-- PAGE TEMPLATE HERE -->
        <style>
        header.wtrHeader.wtrAnimate.wtrHeaderColor.wtrFullWidthHeader {
            display: none;
        }

        @media(min-width: 1200px) {

            .wtrFooterContainer.wtrFooterColor.wtrFooterWdg,
            .wtrCopyright.wtrCopyrightColor {
                display: none;
            }

            .active-subscriber-or-super-admin div#mp-pusher {
                height: 100vh !important;
            }
        }
        </style>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
            integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo THEME_URL; ?>/subscription-src/includes/video-modal.css">
        <link rel="stylesheet" href="<?php echo THEME_URL; ?>/subscription-src/style.css">

        <?php
        wp_head();
        ?>

    </head>

    <body class="custom-lp-temp dashboard">
        <div class="larger-wrapper">
            <header class="dashboard-header">
                <div class="header-wrapper container">
                    <div class="page-tint hide-after-1200"></div>

                    <a href="<?php echo home_url('dashboard'); ?>">
                        <div class="logo-box hide-after-1200">
                            <img src="<?php echo THEME_URL; ?>/images/endorphitness-03.png" alt="oneims logo" />
                        </div>
                    </a>

                    <div id="nav-drawer" class="drawer">
                        <div class="drawer-inner">
                            <div class="drawer-logo-wrapper">
                                <a href="<?php echo home_url('dashboard'); ?>">
                                    <img src="<?php echo THEME_URL; ?>/images/endorphitness-03.png"
                                        alt="oneims globe" />
                                </a>
                            </div>

                            <nav class="list-wrapper">
                                <ul>
                                    <li class="has-sub-navigation show-my-children">
                                        <div class="flex-icon">
                                            <i class="fas fa-dumbbell"></i>
                                            <a
                                                href="<?php echo home_url('workouts/'); ?>"><?php the_field('workouts_navigation_link_text', 'option'); ?></a>
                                        </div>
                                        <ul style="display: block;">
                                            <!-- Loop Here -->
                                            <?php
									$args = array(  
										'post_type' => 'workout',
										'post_status' => 'publish',
										'posts_per_page' => 5,
										'orderby' => 'date',
										'order'   => 'DESC',
									);

									// Get current page and append to custom query parameters array
									// $args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

								$loop = new WP_Query( $args );

								$temp_query = $wp_query;
								$wp_query   = NULL;
								$wp_query   = $loop;
								//reverse the order of the posts, latest last
								$array_rev = array_reverse($loop->posts);
								$loop->posts = $array_rev;
								// The Loop
								if ( $loop->have_posts() ) :
									while($loop->have_posts()) : $loop->the_post(); 
										?>
                                            <li><a
                                                    href="<?= get_permalink(get_the_ID()); ?>"><?= get_the_title(); ?></a>
                                            </li>
                                            <?php
											$i++;
											endwhile;
											wp_reset_query();
											?>
                                            <?php
										endif;
									?>
                                        </ul>
                                    </li>
                                    <?php 
                                    $args = array(  
                                        'post_type' => 'grocery',
                                        'post_status' => 'publish',
                                        'posts_per_page' => 1,
                                        'orderby' => 'date',
                                        'order'   => 'DESC',
                                    );
                                    $loop = new WP_Query( $args );
                                    if ( $loop->have_posts() ) : 
                                    while ( $loop->have_posts() ) : $loop->the_post(); 
                                    ?>
                                    <li>
                                        <?php if (plan_starter()) {?>
                                        <div class="flex-icon">
                                            <i class="fas fa-lock"></i>
                                            <a href="<?php echo home_url($collective_module_url); ?>"
                                                data-tippy-content="Upgrade to view Grocery List"
                                                class="no-hover-color">Grocery List <span style="top: 1px;"
                                                    class="upgrade-cta-nav">(Upgrade)</span></a>
                                        </div>
                                        <?php } else { ?>
                                        <div class="flex-icon">
                                            <i class="fas fa-list-alt"></i>
                                            <a href="<?= get_permalink(get_the_ID()); ?>">Menu & Grocery List</a>
                                        </div>
                                        <?php } ?>
                                    </li>
                                    <?php 
                                    endwhile;
                                    endif; ?>

                                    <li>
                                        <div class="flex-icon">
                                            <i class="fas fa-utensils"></i>
                                            <a
                                                href="<?php echo home_url('recipes/'); ?>"><?php the_field('recipes_navigation_link_text', 'option'); ?></a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="flex-icon">
                                            <i class="fas fa-comments"></i>
                                            <a href="https://www.facebook.com/groups/439071286775389/?source_id=123994671587487"
                                                target="_blank"><?php the_field('forum_navigation_link_text', 'option'); ?></a>
                                        </div>
                                    </li>

                                    <li class="has-sub-navigation">
                                        <div class="flex-icon">
                                            <i class="fas fa-running"></i>
                                            <a
                                                href="<?php echo home_url('stretching-mobility/'); ?>"><?php the_field('stretching_&_mobility_link_text', 'option'); ?></a>
                                        </div>
                                        <ul>
                                            <?php
									$args = array(  
										'post_type' => 'stretching',
										'post_status' => 'publish',
										'posts_per_page' => -1,
										'orderby' => 'date',
										'order'   => 'DESC',
									);

								$loop = new WP_Query( $args );
								if ( $loop->have_posts() ) : 
									while($loop->have_posts()) : $loop->the_post(); 
										?>
                                            <li><a href="<?= get_permalink(get_the_ID()); ?>"><?php the_title(); ?></a>
                                            </li>
                                            <?php
											$i++;
									endwhile;
									wp_reset_postdata();
								?>
                                            <?php
									endif;
								?>
                                        </ul>
                                    </li>
                                    <?php if( have_rows('new-link-repeater', 'option') ):
                            while ( have_rows('new-link-repeater', 'option') ) : the_row(); ?>
                                    <li>
                                        <div class="flex-icon">
                                            <?php the_sub_field('link-icon'); ?>
                                            <a
                                                href="<?php the_sub_field('link-url'); ?>"><?php the_sub_field('link-text'); ?></a>
                                        </div>
                                    </li>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                    <li>
                                        <div class="flex-icon">
                                            <?php if (plan_starter()) {?>
                                            <i class="fas fa-lock"></i>
                                            <?php } else { ?>
                                            <i class="fas fa-calendar-week"></i>
                                            <?php } ?>
                                            <a href="<?php 
                                            if (plan_starter()) {
                                                echo home_url($collective_module_url);
                                            } else { 
                                                echo home_url('checkin');
                                            } 
                                            ?>" <?php if (plan_starter()) {?>data-tippy-content=" Upgrade to enable
                                                Check-in" class="no-hover-color" <?php } ?>>Check-In
                                                <?php if (plan_starter()) {?><span
                                                    class="upgrade-cta-nav">(Upgrade)</span><?php }?></a>
                                        </div>
                                    </li>
                                </ul>
                            </nav>

                            <div class="shout-out-wrapper hide-after-1200">
                                <ul>
                                    <li><a href="<?php echo home_url('my-account'); ?>">Manage Account</a></li>
                                    <li><a href="<?php echo home_url('my-account/customer-logout'); ?>">Log Out</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="hamburger-parent d-wrapper hide-after-1200">
                        <button role="button" class="d-btn site-menu-toggle js-toggle-menu">
                            <span class="hamburger-line hamburger-line--top"></span>
                            <span class="hamburger-line hamburger-line--middle"></span>
                            <span class="hamburger-line hamburger-line--bottom"></span>
                        </button>
                    </div>
                </div>
            </header>