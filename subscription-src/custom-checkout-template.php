<?php 
function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

global $wp;
global $userdata, $user_identity;
get_currentuserinfo();
// Local
// $group_product_id = '15266';
// $generic_variation_id = '15268';
// $variation_id_with_trial = '15268';

// Live
$group_product_id = '15788';
$generic_variation_id = '15790';
$variation_id_with_trial = '15790';

$current_url = home_url(add_query_arg(array($_GET), $wp->request));
$url_array = parse_url($current_url);
$path = $url_array['path'].'?'.$url_array['query'];
$key = 'handle';
$filteredURL = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $path);
$current_group_id = $_GET['add-to-cart'];
$switchSubscriptionParam = $_GET['switch-subscription'];
$current_variation_id = get_string_between($current_url, '%5B', '%5D');
$duplicated_variation = false;
$defaultURL = 'checkout-subscription/?add-to-cart='.$group_product_id.'&quantity['.$generic_variation_id.']=1&handle=step_one';


// Test URL: http://localhost:10003/checkout-subscription/?add-to-cart=15266&quantity%5B15268%5D=1&handle=step_one
// Test Switch URL: http://localhost:10003/checkout-subscription/?add-to-cart=15266&quantity%5B15268%5D=1&switch-subscription=15282&item=6&_wcsnonce=3452620425&handle=step_two

if (wcs_user_has_subscription('', '', '') && !wcs_user_has_subscription('', '', 'active') && !has_active_subscription()) {
    wp_redirect(home_url('my-account/subscriptions?msg=You already have an inactive subscription. Please resubscribe to switch/activate the plan. Please contact us if you are having any trouble.')); 
}

if (get_query_var('handle')) {
    $handle = get_query_var('handle');
} else {
    wp_redirect(home_url($defaultURL));
    exit();
}

if ($current_group_id !== $group_product_id) {
    wp_redirect(home_url($defaultURL));
    exit();
}



$user_subscriptions = wcs_get_users_subscriptions();
if  ( has_active_subscription() ) {
    $subscription_id;
    $variation_id;
    $item;
    $_wcsnonce;
    if( ! empty( $user_subscriptions ) ) {
        foreach( $user_subscriptions as $user_subscription_id => $subscription ) {
            if ($subscription->has_status(array('active'))) {
                $subscription_id = $subscription->get_id();
            }
            foreach( $subscription->get_items() as $item_line_number => $item_arr ) {
                if( $subscription_id == $item_arr['order_id'] ) {
                    $item = $item_line_number;
                    $_wcsnonce = wp_create_nonce( 'wcs_switch_request' );
                    $variation_id = $item_arr['variation_id'];
                }
            }
        }
    }
    if ($variation_id == $current_variation_id) {
        $duplicated_variation = true;
    }

    if (!$switchSubscriptionParam) {
        $filteredURL = $filteredURL.'switch-subscription='.$subscription_id.'&item='.$item.'&_wcsnonce='.$_wcsnonce.'&';
    }
} else {
    foreach ($user_subscriptions as $subscription){
        if ($subscription->has_status(array('on-hold'))) {
            wc_clear_notices();
            wp_redirect(home_url('my-account/subscriptions?msg=You already have an inactive subscription. Please resubscribe to switch/activate the plan')); 
            exit();
        }
      }
}


if (!is_user_logged_in() && $handle !== 'step_one') {
    wp_redirect(home_url($filteredURL.'handle=step_one'));
    exit();
} elseif (is_user_logged_in() && $handle !== 'step_two' && is_user_logged_in() && $handle !== 'step_three') {
    wp_redirect(home_url($filteredURL.'handle=step_two'));
    exit();
}
?>

<?php
/**
 * Template Name: Subscription Checkout Template
 */

get_template_part('templates/particals', 'header');

?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/subscription-src/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/subscription-src/custom-checkout.css">
<?php 
$cartCount = WC()->cart->get_cart_contents_count();
if ($cartCount < 2) {?>
<style>
li.wtrNaviItem.wtrNaviCartItem {
    display: none;
}

@media (min-width: 992px) {
    body {
        overflow: unset;
    }
}
</style>
<?php }
?>

<main class="custom-lp-temp checkout-page">
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <!-- Content -->
                    <?php if( have_rows('main_content') ):
		            while ( have_rows('main_content') ) : the_row(); ?>
                    <div class="content-wrapper text-left mb-4">
                        <?php if( get_sub_field('heading') ): ?>
                        <h1 class="text-green content-wrapper__title"><?php the_sub_field('heading'); ?></h1>
                        <?php endif; ?>
                        <?php if( get_sub_field('description') ): ?>
                        <p><?php the_sub_field('description'); ?></p>
                        <?php endif; ?>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- Navigation -->
                    <div class="checkout-navigation">
                        <div class="checkout-navigation__wrapper">
                            <!--  -->
                            <div class="checkout-navigation__column active">
                                <div class="checkout-navigation__content-box">
                                    <div class="checkout-navigation__title-wrapper">
                                        <h3 class="checkout-navigation__title">Step 1</h3>
                                        <h4 class="checkout-navigation__label">Begin Registration</h4>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="checkout-navigation__column <?php if ($handle === 'step_two' || $handle === 'step_three') {?>active<?php }?> <?php if ($handle === 'step_three') {?>cursor-pointer<?php }?>"
                                <?php if ($handle === 'step_three') {?>onclick="location.href='<?php echo $filteredURL.'handle=step_two'; ?>'"
                                <?php }?>>
                                <div class="checkout-navigation__content-box">
                                    <div class="checkout-navigation__title-wrapper">
                                        <h3 class="checkout-navigation__title">Step 2</h3>
                                        <h4 class="checkout-navigation__label">Review</h4>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="checkout-navigation__column <?php if ($handle === 'step_three') {?>active<?php }?> <?php if ($handle === 'step_two') {?>cursor-pointer<?php }?> <?php if ($duplicated_variation) { ?>duplicated-variation<?php } ?>"
                                <?php if ($handle === 'step_two') {?>onclick="location.href='<?php echo $filteredURL.'handle=step_three'; ?>'"
                                <?php }?>>
                                <div class="checkout-navigation__content-box">
                                    <div class="checkout-navigation__title-wrapper">
                                        <h3 class="checkout-navigation__title">Step 3</h3>
                                        <?php if (!$switchSubscriptionParam) {?>
                                        <h4 class="checkout-navigation__label">Complete Account</h4>
                                        <?php } else { ?>
                                        <h4 class="checkout-navigation__label">Billing</h4>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>
                    <!-- Checkout form -->
                    <div class="custom-checkout-form" style="display: none;">
                        <div class="custom-checkout-form__wrapper">
                            <?php if ($handle === 'step_one') {?>
                            <div class="custom-checkout-form__step-1">
                                <style>
                                .u-column1 {
                                    display: none;
                                }
                                </style>
                                <?php echo do_shortcode('[woocommerce_my_account]'); ?>
                            </div>
                            <?php } elseif ($handle === 'step_two') {?>
                            <div class="custom-checkout-form__step-2">
                                <?php if ($switchSubscriptionParam) {?>
                                <style>
                                .woocommerce-notices-wrapper {
                                    display: none;
                                }
                                </style>
                                <?php } ?>
                                <?php if ($duplicated_variation) { ?>
                                <div class="custom-checkout-form__content-wrapper">
                                    <div class="woocommerce">
                                        <div class="woocommerce-notices-wrapper" style="display: block;">
                                            <ul class="woocommerce-error" role="alert">
                                                <li style="font-weight: 900;">
                                                    Sorry, <?php echo $current_user->user_firstname; ?>. You can not
                                                    switch to the same subscription. Please choose a new
                                                    subscription
                                                </li>
                                            </ul>
                                        </div>
                                        <a href="<?php echo home_url($collective_module_url); ?>"><button type="button"
                                                class="chh-button">Go
                                                Back and Choose a New Subscription</button></a>
                                    </div>
                                </div>
                                <?php } else {?>
                                <div class="custom-checkout-form__content-wrapper">
                                    <h4 class="text-purple custom-checkout-form__content-wrapper__title">
                                        <?php if ($switchSubscriptionParam) {?>
                                        Awesome
                                        <?php echo $current_user->user_firstname; ?>, Please review your new
                                        plan below!</h4>
                                    <?php } else { ?>
                                    Awesome
                                    <?php echo $current_user->user_firstname; ?>, let's begin with our fitness
                                    journey!</h4>
                                    <?php } ?>
                                </div>
                                <?php echo do_shortcode('[woocommerce_cart]'); ?>
                                <?php } ?>
                            </div>
                            <?php } elseif ($handle === 'step_three') {?>
                            <div class="custom-checkout-form__step-3">
                                <?php if ($switchSubscriptionParam) {?>
                                <style>
                                .woocommerce-notices-wrapper {
                                    display: none;
                                }

                                .shop_table .cart_item .subscription-price,
                                .shop_table .cart-subtotal {
                                    display: none;
                                }
                                </style>
                                <script>
                                function changeSubtotalText() {
                                    jQuery('.shop_table .order-total:not(.recurring-total) th').text(
                                        "Due Today");
                                }
                                jQuery('document').ready(function() {
                                    changeSubtotalText()
                                })

                                jQuery('body').on('updated_checkout', function() {
                                    changeSubtotalText()
                                });
                                </script>
                                <?php } ?>
                                <?php if ($duplicated_variation) { ?>
                                <div class="custom-checkout-form__content-wrapper">
                                    <div class="woocommerce">
                                        <div class="woocommerce-notices-wrapper">
                                            <ul class="woocommerce-error" role="alert">
                                                <li style="font-weight: 900;">
                                                    Sorry, <?php echo $current_user->user_firstname; ?>. You can not
                                                    switch to the same subscription. Please choose a new
                                                    subscription
                                                </li>
                                            </ul>
                                        </div>
                                        <a href="<?php echo home_url($collective_module_url); ?>"><button type="button"
                                                class="chh-button">Go
                                                Back and Choose a New Subscription</button></a>
                                    </div>
                                </div>
                                <?php } else {?>
                                <div class="custom-checkout-form__content-wrapper">
                                    <h4 class="text-purple custom-checkout-form__content-wrapper__title">We're excited,
                                        <?php echo $current_user->user_firstname; ?>!
                                    </h4>
                                    <h5 class="text-purple custom-checkout-form__content-wrapper__subtitle">Please
                                        review your billing details
                                        below and we'll be on our way!</h5>
                                </div>
                                <?php echo do_shortcode('[woocommerce_checkout]'); ?>
                                <?php } ?>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                    <!--  -->
                </div>
                <?php if( have_rows('sidebar_testimonial') ):
		        while ( have_rows('sidebar_testimonial') ) : the_row(); ?>
                <div class="col-lg-4">
                    <div class="content-box">
                        <div class="ui-card bg-white">
                            <div class="top-content">
                                <?php if( get_sub_field('image') ): ?>
                                <?php $imageID = get_sub_field('image'); ?>
                                <figure>
                                    <img class="lozad" src="<?php echo $imageID['url']; ?>"
                                        alt="<?php echo $imageID['alt']; ?>" />
                                </figure>
                                <?php endif; ?>
                            </div>
                            <div class="bottom-content">
                                <?php if( get_sub_field('content') ): ?>
                                <h3><?php the_sub_field('content'); ?></h3>
                                <?php endif; ?>
                                <?php if( get_sub_field('name') ): ?>
                                <h4 class="text-purple"><?php the_sub_field('name'); ?></h4>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
</main>
<script>
const messageOne = jQuery('.woocommerce-form-coupon-toggle');
const messageTwo = jQuery('form.checkout_coupon.woocommerce-form-coupon');
const messageThree = jQuery('.woocommerce-form-login-toggle');
const messageFour = jQuery('form.woocommerce-form.woocommerce-form-login.login');
jQuery('.col-lg-4 .content-box').append(messageOne);
jQuery('.col-lg-4 .content-box').append(messageTwo);
jQuery('.col-lg-4 .content-box').append(messageThree);
// jQuery('.col-lg-4 .content-box').append(messageFour);
messageOne.show();
// messageThree.show();
function addCss(fileName) {
    var link = $("<link />", {
        rel: "stylesheet",
        type: "text/css",
        href: fileName
    })
    $('head').append(link);
}

// New

jQuery('.remove-me').each(function() {
    jQuery(this).parent().addClass('remove-me-parent');
});
const errorLength = jQuery('.remove-me').parent().parent().find('li').length;
const wasteErrorsLength = jQuery('.remove-me-parent').length;
const currentURL = window.location.href;
if (errorLength < 2 || errorLength === wasteErrorsLength) {
    jQuery('.remove-me').parent().parent().remove();
} else {
    jQuery('.remove-me').parent().remove();
}

jQuery('.woocommerce-error li').each(function() {
    const errorText = jQuery(this).text();
    console.log(errorText)
    if (errorText.indexOf('log in') !== -1 || errorText.indexOf(
            'The password you entered for the email address') !== -1) {
        jQuery('#customer_login .u-column1').show();
        jQuery('#customer_login .u-column2').hide();
    } else {
        jQuery('#customer_login .u-column1').hide();
        jQuery('#customer_login .u-column2').show();
    }
});
jQuery('.actions button').text('Continue').attr('type', 'button').click(() => {
    window.location.href = '<?php echo $filteredURL.'handle=step_three'; ?>'
});

jQuery('#order_review_heading').text('Payment Options')

jQuery('.custom-checkout-form').show();
// window.onload = () => {
//     jQuery('.custom-country-field').select2();
// }
</script>
<?php
get_template_part('templates/particals', 'footer');
?>