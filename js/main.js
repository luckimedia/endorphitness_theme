$(document).ready(function () {
  // back to top button js
  $("#scroll").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });

  // testimonial-slider
  $(".testimonial-slider").slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    prevArrow:
      '<button class="slide-arrow prev-arrow">&#8592; Previous</button>',
    nextArrow: '<button class="slide-arrow next-arrow">Next &#8594;</button>',
  });

  $(".testimonial-slider--double").slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    adaptiveHeight: true,
    prevArrow:
      '<button class="slide-arrow prev-arrow">&#8592; Previous</button>',
    nextArrow: '<button class="slide-arrow next-arrow">Next &#8594;</button>',

    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });

  // video popup
  var url = $("#glassAnimalsVideo").attr("src");

  $("#glassAnimals").on("hide.bs.modal", function () {
    $("#glassAnimalsVideo").attr("src", "");
  });
  $("#glassAnimals").on("show.bs.modal", function () {
    $("#glassAnimalsVideo").attr("src", url);
  });
});

// blog slider

$(document).ready(function () {
  $(".blog-post-carousel").slick({
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    dots: true,

    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
});

// social icon sidebar

$(function () {
  if ($("#sidebar").length) {
    var top =
      $("#sidebar").offset().top -
      parseFloat($("#sidebar").css("marginTop").replace(/auto/, 0));
    var footTop =
      $(".sticky-bar").offset().top -
      parseFloat($(".sticky-bar").css("marginTop").replace(/auto/, 0));

    var maxY = footTop - $("#sidebar").outerHeight();

    $(window).scroll(function (evt) {
      var y = $(this).scrollTop();
      if (y >= top - $(".header").height()) {
        if (y < maxY) {
          $("#sidebar").addClass("fixed").removeAttr("style");
        } else {
          $("#sidebar")
            .removeClass("fixed")
            .css({
              position: "absolute",
              top: maxY - top + "px",
            });
        }
      } else {
        $("#sidebar").removeClass("fixed");
      }
    });
  }
});

$(document).ready(function () {
  // variables
  var toTop = $(".to-top");
  // logic
  toTop.on("click", function () {
    $("html, body").animate({ scrollTop: 0 }, 1000);
  });
});

$(document).ready(function () {
  function toggleNavbarMethod() {
    if ($(window).width() > 768) {
      $(".navbar .dropdown > a").click(function () {
        location.href = this.href;
      });
    } else {
      $(".navbar .dropdown").off("mouseover").off("mouseout");
    }
  }
  toggleNavbarMethod();
  $(window).resize(toggleNavbarMethod);
});

jQuery(function ($) {
  if ($(window).width() > 769) {
    $(".navbar .dropdown > a").click(function () {
      location.href = this.href;
    });
  }
});
