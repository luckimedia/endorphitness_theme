<?php
/**
 * Template Name: Archive Recipes
 *  Template Post Type: Recipes
 */
?>
<!-- App Header -->
<?php include('subscription-src/includes/global-app-header.php'); ?>
<!-- End App header -->

<main class="dashboard-main workouts-overview recipes-overview">
    <section class="section intro">
        <?php include('subscription-src/includes/global-app-header-partial.php'); ?>

        <!-- Middle Section -->
        <div class="panels-parent">
            <?php 
                    global $userdata,$user_identity;
                    get_currentuserinfo();
                ?>
            <div class="container">
                <div class="content-box">
                    <h1 class="text-green">Endorphitness Approved <span class="text-purple">Groceries!</span></h1>
                </div>
            </div>

            <?php
                    $args = array(  
                        'post_type' => 'grocery',
                        'post_status' => 'publish',
                        'posts_per_page' => 6,
                        'orderby' => 'date',
                        'order'   => 'DESC',
                    );
                    $loop = new WP_Query( $args );
                    
                    if ( $loop->have_posts() ) : ?>
            <div class="container other-cards pt-3 pt-lg-4">
                <div class="row">
                    <?php
                                $i=1;
                                $lg=6;
                                if($i==1){$lg=6;}
                                if($i==2){$lg=6;}
                                if($i==3){$lg=6;}
                                if($i>4){$lg=6;}
                                while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="col-md-<?= $lg ?> mb-5">
                        <div class="ui-card bg-gray clickable"
                            onclick="location.href='<?= get_permalink(get_the_ID()); ?>'">
                            <div class="top-content">
                                <figure>
                                    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>
                                    <img src="<?= $featured_img_url ?>" alt="Workout Image">
                                </figure>
                            </div>
                            <div class="bottom-content">
                                <h2 class="text-purple"><?= get_the_title(); ?></h2>
                                <p><?= limit_text(get_first_paragraph(), 35); ?></p>
                                <!-- <p class="text-green"><?php echo get_the_date( 'Y-m-d' ); ?></p> -->
                            </div>
                        </div>
                    </div>
                    <?php endwhile; $i++; ?>
                </div>
            </div>
            <div class="container">
                <div class="pagination- pt-4 pb-4 pt-lg-1 pb-lg-1">
                    <?php 
                                the_posts_pagination(
                                    array(
                                        'prev_text' => __( 'Previous page', 'twentysixteen' ),
                                        'next_text' => __( 'Next page', 'twentysixteen' ),
                                    )
                                );
                                ?>
                </div>
            </div>
            <?php
                    endif;
                ?>
        </div>
        <!-- End Middle Section -->

        <div class="app-footer hide-below-1200">
            <div class="container text-center">
                <h6>Copyright <?php echo date('Y'); ?></h6>
            </div>
        </div>
    </section>
</main>

<!-- App Footer -->
<?php include('subscription-src/includes/global-app-footer.php'); ?>
<!-- End App Footer -->