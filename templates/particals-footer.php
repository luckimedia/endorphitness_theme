<?php
  $footer_logo = get_theme_mod('footer_logo_one');
  $footer_image = get_theme_mod('footer_image');
?>
<!-- Footer Start -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-8">
          <div class="row">
            <div class="col-lg-12">
              <div class="footer__logo">
                <a href="<?php echo get_site_url(); ?>"
                  ><img
                    src="<?php echo $footer_logo ?>"
                    class="img-fluid"
                /></a>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="footer__column">
                <!-- Widget 1 -->
                <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
                <div class="footer__link">
                  <?php dynamic_sidebar( 'sidebar-2' ); ?>
                </div>
                <?php endif; ?>

                <!-- Widget 2 -->
                <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
                <div class="footer__link">
                  <?php dynamic_sidebar( 'sidebar-3' ); ?>
                </div>
                <?php endif; ?>
                
                <!-- Widget 3 -->
                <?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
                <div class="footer__link">
                  <?php dynamic_sidebar( 'sidebar-4' ); ?>
                </div>
                <?php endif; ?>

                <!-- Widget 4 -->
                <?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
                <div class="footer__link">
                  <?php dynamic_sidebar( 'sidebar-5' ); ?>
                </div>
                <?php endif; ?>

                <!-- Widget 5 -->
                <?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
                <div class="footer__link">
                  <?php dynamic_sidebar( 'sidebar-6' ); ?>
                </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="footer__image">
            <img src="<?php echo $footer_image ?>" class="img-fluid" />
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="scroll-to-up">
      <button class="scroll-to-up__back-link" onclick="topFunction()" id="myBtn" title="Go to top">Back to top<i class="fas fa-arrow-up"></i></button>
  </div> -->
  </footer>
<!-- Footer End -->

<script>
document.addEventListener('wpcf7mailsent', function(event) {
    gtag('event', 'click', {
        event_category: 'form_submission',
        event_action: 'general_contact_form'
    });
}, false);
</script>
<?php wp_footer(); ?>
</body>
</html>