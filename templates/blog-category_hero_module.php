<?php 
if(is_category()){

    $category = get_category( get_query_var( 'cat' ) );
    $cat_id = $category->cat_ID;

    // $categories = get_the_category();
    $hero_image = get_field('hero_module_category_image', 'category_'.$cat_id);
    $hero_title = get_field('hero_module_category_title', 'category_'.$cat_id);
    $hero_description = get_field('hero_module_category_description', 'category_'.$cat_id);

    if((empty($hero_image)) && (empty($hero_title)) && (empty($hero_description))){
        get_template_part('templates/blog', 'hero_module');
        return;
    }
}else if(is_tag()){
    $tags = get_queried_object();
    $hero_image = get_field('hero_module_category_image', 'post_tag_'.$tags->term_id);
    $hero_title = get_field('hero_module_category_title', 'post_tag_'.$tags->term_id);
    $hero_description = get_field('hero_module_category_description', 'post_tag_'.$tags->term_id);

    if((empty($hero_image)) && (empty($hero_title)) && (empty($hero_description))){
        get_template_part('templates/blog', 'hero_module');
        return;
    }
}
?>
<section class="hero-module">
    <div class="container-fluid">
        <div class="hero-module__row">
            <div class="hero-module__image">
                <img src="<?php echo $hero_image; ?>" class="img-fluid" style="width: 100%" />
            </div>
            <div class="hero-module__content">
                <h1 class="hero-module__title"><?php echo $hero_title; ?></h1>
                <?php echo $hero_description; ?>
            </div>
        </div>
    </div>
</section>