<?php 
    $day_module_title = get_sub_field('day_life_module_title');
    $day_module_desc = get_sub_field('day_life_module_descriptionm');
?>
<section class="about-pattern-block">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="about-pattern-block__section">
                    <div class="about-pattern-block__heading">
                        <h3 class="about-pattern-block__title"><?php echo $day_module_title; ?></h3>
                    </div>
                    <p><?php echo $day_module_desc; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>