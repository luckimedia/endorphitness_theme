<?php 
    $love_hate_module_title = get_sub_field('accelerator_love_hate_module_title');
    $love_hate_module_sub_title = get_sub_field('accelerator_love_hate_module_sub_title');
    $module_block_lists = get_sub_field('accelerator_module_block_lists');
    $love_hate_module_button = get_sub_field('accelerator_love_hate_module_button');
?>
<section class="accelerator-green-block green">
    <div class="container">
        <h2 class="accelerator-green-block__title"><?php echo $love_hate_module_title; ?></h2>
        <div class="row justify-content-center align-items-center no-gutters">
            <div class="col-lg-8">
                <div class="row justify-content-center align-items-center">
            <div class="col-lg-8">
                <div class="accelerator-green-block__column">
                    <h5 class="accelerator-green-block__heading"><?php echo $love_hate_module_sub_title; ?></h5>
                    <ul class="accelerator-green-block__lists">
                    <?php if($module_block_lists): foreach($module_block_lists as $list): ?>
                        <li><?php echo $list['accelerator_block_list_title']; ?></li>
                    <?php endforeach; endif; ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
            <?php if($love_hate_module_button): $target = empty($love_hate_module_button['target']) ? '_self' : '_blank'; ?>
                <a href="<?php echo $love_hate_module_button['url']; ?>" target="<?php echo $target; ?>" class="border-btn"><?php echo $love_hate_module_button['title']; ?></a>
            <?php endif; ?>
            </div>
                </div>
        </div>
        </div>
    </div>
</section>