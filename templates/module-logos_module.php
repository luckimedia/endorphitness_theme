<?php 
    $logo_module_title = get_sub_field('logo_module_title');
    $logos = get_sub_field('logos');
?>
<section class="logos-module">
    <div class="logos-module__row">
        <div >
        <?php if( $logo_module_title ): ?>
            <h4 class="logos-module__title"><?php echo $logo_module_title; ?></h4>
        <?php endif;?>
        </div>
        <div class="logos-module__image">
        <?php if ($logos) : foreach ($logos as $logo) :?>
            <div class="logos-module__img">
                <img src="<?php echo $logo['stream_logo_image']; ?>" class="img-fluid">
            </div>
        <?php endforeach; endif; ?>
        </div>
    </div>
</section>