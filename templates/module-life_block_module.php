<?php 
    $life_main_image = get_sub_field('life_block_module_main_image');
    $life_main_image_position = get_sub_field('life_block_module_main_image_position');
    $vector_image1 = get_sub_field('life_block_module_vector_image_1');
    $vector_image2 = get_sub_field('life_block_module_vector_image_2');
    $description = get_sub_field('life_block_module_description');

?>
<section class="about-image-grey-block grey">
    <div class="container">
        <div class="row justify-content-center no-gutters">

        <?php if($life_main_image_position == 'right'): ?>
            <div class="col-lg-6">
            <?php if($description): ?>
                <div class="about-image-grey-block__desc">
                    <?php echo $description; ?>
                </div>
            <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <div class="about-image-grey-block__right-image">
                    <img src="<?php echo $life_main_image; ?>" class="img-fluid">
                </div>
            </div>
        <?php else: ?>
            <div class="col-lg-6">
                <div class="about-image-grey-block__right-image">
                    <img src="<?php echo $life_main_image; ?>" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-6">
            <?php if($description): ?>
                <div class="about-image-grey-block__desc">
                    <?php echo $description; ?>
                </div>
            <?php endif; ?>
            </div>
        <?php endif; ?>
            <div class="col-lg-6">
                <div class="about-image-grey-block__image">
                    <img src="<?php echo $vector_image1; ?>" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-image-grey-block__image">
                    <img src="<?php echo $vector_image2; ?>" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>