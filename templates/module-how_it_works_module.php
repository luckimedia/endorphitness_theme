<?php 
    $how_it_works_title = get_sub_field('how_it_works_module_title');
    $module_training_steps = get_sub_field('module_training_steps');
?>
<section class="training-steps">
    <div class="container">
        <div class="section-header section-header--small-width">
            <h3 class="section-header__title"><?php echo $how_it_works_title; ?></h3>
        </div>
        <div class="training-steps__module">
        <?php if ($module_training_steps) : $i = 1; foreach ($module_training_steps as $step) :;?>
            <div class="training-steps__column">
                <h5 class="training-steps__title">Step <?php echo $i; ?></h5>
                <div class="training-steps__image <?php echo $step['training_step_border_color']; ?>">
                    <img src="<?php echo $step['training_step_image']; ?>" class="img-fluid">
                </div>
                <h5 class="training-steps__desc"><?php echo $step['training_step_description']; ?></h5>
            </div>
        <?php $i++; endforeach; endif; ?>
        </div>
    </div>
</section>