<?php 
    $programs_module_title = get_sub_field('programs_module_title');
    $programs_module_sub_title = get_sub_field('programs_module_sub_title');
    $title_position = get_sub_field('programs_module_title_position');
    $programs_blocks = get_sub_field('programs_blocks');
    if($title_position == 'right'){
        $order = "1;";
    }else{
        $order = "0;";  
    }
?>
<section class="product-section">
    <div class="container">
        <div class="dubble-heading-section dubble-heading-section--right" >
        <h3 class="dubble-heading-section__title" style="order:<?php echo $order; ?>"><?php echo $programs_module_title; ?></h3>
        <?php if(!empty($programs_module_sub_title)){ ?>
            <h4 class="dubble-heading-section__subtitle"><?php echo $programs_module_sub_title; ?></h4>
        <?php }else{ ?>
        <div class="blank_div"></div>
        <?php } ?>
        </div>
        <div class="row">
        <?php if ($programs_blocks) : foreach ($programs_blocks as $program) :?>
            <div class="col-lg-4">
                <div class="product-column" style="background-color: <?php echo $program['block_color']; ?>">
                <div class="product-column__section">
                <h4 class="product-column__title"><?php echo $program['programs_block_title']; ?></h4>
                <?php if($program['program_block_content_or_image'] == 'p_content'): ?>
                    <div class="product-column__content">
                        <p><?php echo $program['program_block_content']; ?></p>
                    </div>
                <?php elseif($program['program_block_content_or_image'] == 'p_image'): ?>
                    <div class="product-column__img">
                        <img src="<?php echo $program['program_block_image']; ?>" class="img-fluid">
                    </div>
                <?php endif; ?>
                </div>
                <div class="product-column__button">
                <div class="product-column__btn">
                <?php if(!empty($program['program_block_button']['title'])): ?>
                    <a href="<?php echo $program['program_block_button']['url']; ?>" class="btn btn--green" style="background-color: <?php echo $program['block_color']; ?>"><?php echo $program['program_block_button']['title']; ?></a>
                <?php endif; ?>
                </div>
                </div>
                </div>
            </div>
        <?php endforeach; endif; ?>
        </div>
    </div>
</section>