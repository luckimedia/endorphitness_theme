<?php 
    $left_right_module_image = get_sub_field('left_right_module_image');
    $left_right_module_description = get_sub_field('left_right_module_description');
    $left_right_module_image_position = get_sub_field('left_right_module_image_position');
    $display_columns = get_sub_field('left_right_module_display_columns');
    if($display_columns == 1){
        $column_class1 = 'col-lg-4';
        $column_class2 = 'col-lg-8';
    }else if($display_columns == 2){
        $column_class1 = 'col-lg-6';
        $column_class2 = 'col-lg-6';
    }else if($display_columns == 3){
        $column_class1 = 'col-lg-5';
        $column_class2 = 'col-lg-7';
    }
    $title = get_sub_field('left_right_module_title');
    $button = get_sub_field('left_right_module_button');

    $left_right_module_title_font_family = get_sub_field('left_right_module_title_font_family');
    if($left_right_module_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($left_right_module_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($left_right_module_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $left_right_module_title_font_color = get_sub_field('left_right_module_title_font_color');
    $left_right_module_title_border_bottom = get_sub_field('left_right_module_title_border_bottom');
?>
<section class="left-right-image-module left-right-image-module--<?php echo $left_right_module_image_position; ?>">
    <div class="container">
        <div class="row align-items-center">
            <div class="<?php echo $column_class1; ?> left-right-image-module__img">
                <div class="left-right-image-module__image">
                    <img src="<?php echo $left_right_module_image; ?>" class="img-fluid" style="width: 100%;">
                </div>
            </div>
            <div class="<?php echo $column_class2; ?> left-right-image-module__info">
                <div class="left-right-image-module__text">
                    <!-- <h5 class="left-right-image-module__subtitle"><?php //echo $title; ?></h5> -->
                    <h5 class="left-right-image-module__heading left-right-image-module__heading--<?php echo $font; ?>"><?php echo $title; ?></h5>
                    <p><?php echo $left_right_module_description; ?></p>
                    <?php if($button): ?>
                        <a href="<?php echo $button['url']; ?>" class="btn"><?php echo $button['title']; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>