<?php 
    $accordion_title = get_sub_field('accordion_title');
    $accordion_description = get_sub_field('accordion_description');
    $accordions = get_sub_field('accordions');
    $button = get_sub_field('accordion_join_button');
?>
<div class="accordion-module">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="accordion-module__heading">
                    <h5 class="accordion-module__title"><?php echo $accordion_title; ?></h5>
                    <p><?php echo $accordion_description; ?></p>
                    <?php if($button): ?>
                        <a href="<?php echo $button['url']; ?>" class="btn"><?php echo $button['title']; ?></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="accordion-section" id="accordionExample">
                <?php if ($accordions) : $i = 0 ;foreach ($accordions as $accordion) :?>
                    <div class="card">
                        <div class="card-header" id="heading<?php echo $i; ?>">
                            <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>"
                                aria-expanded="true" aria-controls="collapseOne">
                                <?php echo $accordion['accordion_heading']; ?>
                            </button>
                        </div>
                        <div id="collapse<?php echo $i; ?>" class="collapse <?php //if($i == 0){ echo 'show'; } ?>" aria-labelledby="heading<?php echo $i; ?>"
                            data-parent="#accordionExample">
                            <div class="card-body">
                                <p><?php echo $accordion['accordion_content']; ?></p>
                            </div>
                        </div>
                    </div>
                <?php $i++; endforeach; endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>