<?php 
    $blog_post_title = get_field('blog_post_title', 'options');
    $center_title_font_family = get_field('blog_post_title_font_family', 'options');
    if($center_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($center_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($center_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $center_title_font_color = get_field('blog_post_title_font_color', 'options');
    $center_content_title_border = get_field('blog_post_title_border_bottom', 'options');
?>
<section class="blog-post-slider">
        <div class="container">
            <div class="section-header">
                <?php if($blog_post_title): ?>
                    <h3 class="<?php if($center_title_font_color == 'green'){ echo 'section-header__title--green ';} else { echo 'section-header__title '; } ?>left-right-image-module__heading--<?php echo $font; ?><?php if($center_content_title_border == 'yes'){ echo ' section-header__title--border-bottom';} ?>"><?php echo $blog_post_title; ?></h3>
                <?php endif; ?>
            </div>
            <div class="blog-post-slider__slider">
                <div class="blog-post-carousel">
                <?php 
                    $lastposts = get_posts(array(
                        // 'posts_per_page' =>  $module['post_per_page'],
                        'order' => 'ASC',
                        'post_type' => 'post'
                    )); 
                    if ( $lastposts ) :
                        foreach ($lastposts as $post) : setup_postdata( $post );  
                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
                ?>
                    <div class="blog-post-column__section">
                        <div class="blog-post-column">
                            <a href="<?php the_permalink(); ?>" class="blog-post-column__image" style="background-image: url(<?php echo $featured_img_url; ?>);">
                            </a>
                            <h4 class="blog-post-column__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div>
                    </div>
                <?php 
                    endforeach;
                    wp_reset_postdata();
                    endif;
                ?>
                </div>
            </div>
        </div>
</section>