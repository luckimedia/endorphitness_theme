<?php 
    $left_right_content_title = get_sub_field('left_right_content_title');
    $left_right_content_description = get_sub_field('left_right_content_description');
    $left_right_content_image = get_sub_field('left_right_content_image');
    $left_right_content_image_position = get_sub_field('left_right_content_image_position');
?>
<section class="left-right-content left-right-content--<?php echo $left_right_content_image_position; ?>">
    <div class="container">
        <div class="row no-gutters align-items-center">
            <div class="col-lg-5 left-right-content__img">
                <div class="left-right-content__image">
                    <img src="<?php echo $left_right_content_image; ?>" class="img-fluid" style="width: 100%;">
                </div>
            </div>
            <div class="col-lg-7 left-right-content__info">
                <div class="left-right-content__text">
                    <h5 class="left-right-content__title"><?php echo $left_right_content_title; ?></h5>
                    <p><?php echo $left_right_content_description; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>