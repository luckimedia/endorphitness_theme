<?php
    $blog_filter_title = get_field('blog_filter_title_option', 'options');
    $center_title_font_family = get_field('blog_filter_title_font_family_option', 'options');
    if($center_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($center_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($center_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $center_title_font_color = get_field('blog_filter_title_font_color_option', 'options');
    $center_content_title_border = get_field('blog_filter_title_border_bottom_option', 'options');

    if(is_category()){
        $category = get_category( get_query_var( 'cat' ) );
        $cat_id = $category->cat_ID;
        // $categories = get_the_category();
        $blog_filter_tags = get_field('category_filter_tags', 'category_'.$cat_id);
    }else{
        $blog_filter_tags = get_field('blog_filter_tags_option', 'options');
    }
?>
<section class="blog-filter-module">
    <div class="container">
        <div class="section-heading">
        <h3 class="<?php if($center_title_font_color == 'green'){ echo 'section-header__title--green ';} else { echo 'section-header__title '; } ?>left-right-image-module__heading--<?php echo $font; ?><?php if($center_content_title_border == 'yes'){ echo ' section-header__title--border-bottom';} ?>"><?php echo $blog_filter_title; ?></h3>
        </div>
        <div class="blog-filter-module__section">
            <div class="row">
            <?php if ($blog_filter_tags) : foreach ($blog_filter_tags as $tag) : 
                $slug =  get_site_url().'/'.'tag/'.$tag->slug;

                // Get the current tag image
                $tag_image = z_taxonomy_image_url($tag->term_id);    
            ?>
                <div class="col-lg-4 col-md-6">
                    <a href="<?php echo $slug; ?>">
                        <div class="blog-filter-module__block" style="background-image: url(<?php echo $tag_image; ?>);">
                            <h3 class="blog-filter-module__heading"><?php echo $tag->name; ?></h3>
                        </div>
                    </a>
                </div>
            <?php endforeach; endif; ?>
            </div>
        </div>
    </div>
</section>