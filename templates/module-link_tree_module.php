<?php 
    $link_trees = get_sub_field('link_trees');
    $link_tree_thanks_message = get_sub_field('link_tree_thanks_message');
?>
<section class="link-tree-module">
    <div class="link-tree-module--pattern">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="row">
                    <?php if ($link_trees) : foreach ($link_trees as $link_tree) :?>
                        <div class="col-lg-6 col-md-6">
                            <div class="link-tree-module__column">
                                <div class="link-tree-module__info">
                                    <h4 class="link-tree-module__heading"><?php echo $link_tree['link_tree_title']; ?></h4>
                                </div>
                                <ul class="link-tree-module__btns">
                                <?php if ($link_tree['link_tree_buttons']) : foreach ($link_tree['link_tree_buttons'] as $link_tree_btn) :?>
                                <?php $target = empty($link_tree_btn['link_tree_button']['target']) ? '_self' : '_blank'; ?>
                                    <li><a href="<?php echo $link_tree_btn['link_tree_button']['url']; ?>" target="<?php echo $target; ?>" class="btn"><?php echo $link_tree_btn['link_tree_button']['title']; ?></a></li>
                                <?php endforeach; endif; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endforeach; endif; ?>
                    </div>                      
                </div>
            </div>
        </div>
    </div>
    <div class="link-tree-module__thanks-msg">
        <p><?php echo $link_tree_thanks_message; ?></p>
    </div>
</section>