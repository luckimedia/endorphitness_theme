<?php 
    $devider_image = get_sub_field('devider_image');
?>
<section class="devider devider--grey">
    <img src="<?php echo $devider_image; ?>" class="img-fluid" style="width: 100%"/>
</section>