<?php
$site_url = get_site_url();
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php  $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  
       $category_detail = get_the_category(get_the_ID());
        foreach($category_detail as $cd){
            $cat_name =  $cd->cat_name;
            break;
        }
?>
<section class="blog-details-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="blog-details-content__section">
                    <img src="<?php echo $featured_img_url; ?>" class="img-fluid">
                    <h3 class="blog-details-content__heading small-line"><?php the_title(); ?></h3>
                    <h4 class="blog-details-content__blog-category"><?php echo $cat_name; ?></h4>
                    <div class="blog-details-content__desc-module"><?php the_content(); ?></div>
                    <div class="bio">
                        <div class="bio__image">
                            <img src="<?php echo THEME_URL; ?>/images/t001.jpg" class="img-fluid">
                        </div>
                        <div class="bio__content">
                            <span class="bio__title"><?php echo get_the_author(); ?></span>
                            <span class="bio__subtitle"><?php echo get_the_date(); ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-side-bar" id="sidebar">
                    <div class="search-bar">
                        <form action="<?php echo site_url('/'); ?>" method="get" class="search">
                            <input type="text" name="s" class="searchTerm" placeholder="Search Blog....">
                            <button type="submit" class="searchButton">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <div class="sidebar-social-icons">
                        <h4 class="sidebar-social-icons__heading">Social icons</h4>
                        <ul class="sidebar-social-icons__icons">
                            <li class="facebook"><a href="http://www.facebook.com/sharer.php?u=<?=$site_url?>" target="_blank"><i class="fab fa-facebook-f"></i>Facebook</a></li>
                            <li class="google-plus"><a href="https://plus.google.com/share?url=<?=$site_url?>" target="_blank"><i class="fab fa-google-plus-g"></i>Google Plus</a>
                            </li>
                            <li class="twiter"><a href="http://twitter.com/share?url=<?=$site_url?>&text=Simple Share Buttons&hashtags=simplesharebuttons"><i class="fab fa-twitter"></i>Twitter</a></li>
                            <li class="tumblr"><a href="http://www.tumblr.com/share/link?url=<?=$site_url?>&amp;title=<?php the_title() ?>"><i class="fab fa-tumblr"></i>Tumblr</a></li>
                            <li class="pinterest"><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fab fa-pinterest-p"></i>Pinterest</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="sticky-bar"></div>
<?php
	endwhile;
	the_posts_navigation();
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif;
?>