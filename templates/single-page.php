<?php

if(have_rows('all_modules')) :
    while(have_rows('all_modules')) : the_row();
        get_template_part('templates/module', get_row_layout());
    endwhile;
endif;