<?php 
$post_per_block = get_sub_field('testimonal_per_block');
$testimonials = get_posts([
  'post_type' => 'Testimonial',
  'posts_per_page'=> $post_per_block,
]);

$testimonal_module_title = get_sub_field('testimonal_module_title');
$testimonal_module_sub_title = get_sub_field('testimonal_module_sub_title');
$testimonial_type = get_sub_field('testimonial_type');
$testimonial_slider_type = get_sub_field('testimonial_slider_type');
if($testimonial_slider_type == 'double'){
  $column = '12';
  $double = '--double';
}else{
  $column = '5';
  $double = '';
}
if($testimonial_type == 'image'){
  $testimonal_module_left_image = get_sub_field('testimonal_module_left_image');
  $testimonal_module_right_image = get_sub_field('testimonal_module_right_image');
}
else if($testimonial_type == 'video'){
  $testimonal_module_video_image = get_sub_field('testimonal_module_video_image');
  $testimonal_module_video_url = get_sub_field('testimonal_module_video_url');
}
?>
<div class="testimonal-section">
  <div class="container">
    <div class="dubble-heading-section">
      <h3 class="dubble-heading-section__title"><?php echo $testimonal_module_title; ?></h3>
      <h4 class="dubble-heading-section__subtitle"><?php echo $testimonal_module_sub_title; ?></h4>
    </div>
    <div class="row">
    <?php if ($testimonial_slider_type != 'double'): ?>
      <div class="col-lg-7">
        <?php if ($testimonial_type == 'image'): ?>
          <div class="row">
            <div class="col-sm-6">
              <div class="curv-rectangle">
                <div
                class="curv-rectangle__image"
                style="background-image: url(<?php echo $testimonal_module_left_image; ?>)"
                ></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="curv-rectangle">
                <div
                class="curv-rectangle__image"
                style="background-image: url(<?php echo $testimonal_module_right_image; ?>)"
                ></div>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <?php if ($testimonial_type == 'video'): ?>
          <div class="testimonial-video" style="background-image: url(<?php echo $testimonal_module_video_image; ?>);">
            <button type="button" class="testimonial-video__btn" data-toggle="modal"
            data-target="#testimonial_video"><img src="<?php echo THEME_URL; ?>/images/Polygon-1.png" class="img-fluid"></button>
          </div>
          <div class="modal fade testimonial-video-modal" id="testimonial_video" tabindex="-1" role="dialog"
          aria-labelledby="exampleModalLabel" aria-hidden="true">Polygon 1.png
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <iframe width="100%" height="315" src="<?php echo $testimonal_module_video_url ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <!-- <video controls id="video1" style="width: 100%;">
                  <source
                  src="<?php echo $testimonal_module_video_url; ?>"
                  type="video/mp4">
                  Your browser doesn't support HTML5 video tag.
                </video> -->
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>

    <div class="col-lg-<?php echo $column; ?>">
      <div class="testimonial-slider<?php echo $double; ?>">
        <?php if ($testimonials) : foreach ($testimonials as $testimonial) :
          $id = $testimonial->ID;
          $featured_img_url = get_the_post_thumbnail_url($id,'full');
          $content = get_post_meta( $id, '_testimonial_info', true );  
          $star_count = get_field('testimonial_rating', $id);
          if($star_count == '1star'){
            $star = '1stars.png';
          }else if($star_count == '2star'){
            $star = '2stars.png';
          }
          else if($star_count == '3star'){
            $star = '3stars.png';
          }
          else if($star_count == '4star'){
            $star = '4stars.png';
          }
          else if($star_count == '5star'){
            $star = '5stars.png';
          }
          ?>
          <div class="testimonial-block">
            <div class="testimonial-block__rate">
              <img src="<?php echo THEME_URL; ?>/images/<?php echo $star; ?>" class="img-fluid" />
            </div>
            <p>
              <?php echo $content['quote']; ?>
            </p>
            <div class="bio">
              <div class="bio__image">
                <img src="<?php echo $featured_img_url; ?>" class="img-fluid" />
              </div>
              <div class="bio__content">
                <span class="bio__title"><?php echo $content['user_name']; ?></span>
                <span class="bio__subtitle"><?php echo $content['user_location']; ?></span>
              </div>
            </div>
          </div>
        <?php endforeach; endif; ?>
      </div>
    </div>
  </div>
</div>
</div>