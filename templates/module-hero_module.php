<?php 
    $hero_image = get_sub_field('hero_image');
    $hero_title = get_sub_field('hero_title');
    $hero_sub_title = get_sub_field('hero_sub_title');
    $hero_description = get_sub_field('hero_description');
    if($want_to_disaply_button = get_sub_field('want_to_disaply_button')){
        $hero_button = get_sub_field('hero_button');
    }
?>
<section class="hero-module">
    <div class="container-fluid">
        <div class="hero-module__row">
            <div class="hero-module__image">
                <img src="<?php echo $hero_image; ?>" class="img-fluid" style="width: 100%" />
            </div>
            <div class="hero-module__content">
                <?php if($hero_sub_title): ?>
                    <span class="hero-module__subtitle left-right-image-module__heading--playlist"><?php echo $hero_sub_title; ?></span>
                <?php endif; ?>
                <h1 class="hero-module__title"><?php echo $hero_title; ?><h1>
                <p>
                    <?php echo $hero_description; ?>
                </p>
                <?php if(($want_to_disaply_button == 'yes') && (($hero_button))): ?> 
                <div class="hero-module__button"> <a href="<?php echo $hero_button['url']; ?>" class="btn btn--green"><?php echo $hero_button['title']; ?></a></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>