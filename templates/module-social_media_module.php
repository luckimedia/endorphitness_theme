<?php 
    $content_title = get_sub_field('content_title');
    $content_image = get_sub_field('content_image');
    $content_description = get_sub_field('content_description');
    $social_media_buttons = get_sub_field('social_media_buttons');
?>
<section class="media-content">
    <div class="container">
    <div class="row align-items-center">
        <div class="col-lg-5">
        <div class="media-content__img">
            <img src="<?php echo $content_image; ?>" class="img-fluid" />
        </div>
        </div>
        <div class="col-lg-5">
        <div class="media-content__text">
            <h3 class="media-content__title"><?php echo $content_title; ?></h3>
            <p><?php echo $content_description; ?></p>
            <ul class="media-content__btns">
            <?php if($social_media_buttons): 
                foreach($social_media_buttons as $social): ?>
            <li>
                <a href="<?php echo $social['social_button_link']['url']; ?>" class="btn"
                ><i class="fab fa-<?php echo $social['social_button_link']['title']; ?>"></i
                ><span><?php echo $social['social_button_title']; ?></span></a
                >
            </li>
            <?php endforeach; endif; ?>
            </ul>
        </div>
        </div>
        <div class="col-lg-2">
        <div class="media-content-menu media-content-menu--phone">
            <?php if ( is_active_sidebar( 'media-sidebar' ) ) : ?>
                <?php dynamic_sidebar( 'media-sidebar' ); ?>
            <?php endif; ?>
            <a href="#" class="media-content-menu__back-link" id="scroll"
            >Back to top<i class="fas fa-arrow-up"></i
            ></a>
        </div>
        </div>
    </div>
    </div>
    <div class="media-content-menu media-content-menu--desk">
        <?php if ( is_active_sidebar( 'media-sidebar' ) ) : ?>
            <?php dynamic_sidebar( 'media-sidebar' ); ?>
        <?php endif; ?>
        <!-- <div class="scroll-to-up">
      <button class="scroll-to-up__back-link" onclick="topFunction()" id="myBtn" title="Go to top">Back to top<i class="fas fa-arrow-up"></i></button>
  </div> -->
    <a href="#" class="media-content-menu__back-link to-top" id="scroll"
        >Back to top<i class="fas fa-arrow-up"></i
    ></a>

    <!-- <span class="to-top">Scroll to Top</span> -->
    </div>
</section>