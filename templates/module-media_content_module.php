<?php 
    $media_content_title = get_sub_field('media_content_title');
    $media_content_sub_title = get_sub_field('media_content_sub_title');
    $media_content_description = get_sub_field('media_content_description');
    $media_content_image = get_sub_field('media_content_image');
    $media_image_position = get_sub_field('media_image_position');
?>
<div class="content-media-module content-media-module--<?php echo $media_image_position; ?>">
    <div class="container">
        <div class="dubble-heading-section">
            <h3 class="dubble-heading-section__title"><?php echo $media_content_title; ?></h3>
            <h4 class="dubble-heading-section__subtitle"><?php echo $media_content_sub_title; ?></h4>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-7 content-media-module-text">
            <div class="content-media-module__text">
                <p><?php echo $media_content_description; ?></p>
            </div>
            </div>
            <div class="col-lg-5 content-media-module-img">
            <div class="curv-rectangle">
                <div
                class="curv-rectangle__image"
                style="background-image: url(<?php echo $media_content_image; ?>)"
                ></div>
            </div>
            </div>
        </div>
    </div>
</div>