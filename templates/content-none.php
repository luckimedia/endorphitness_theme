<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package endorphitness
 */

?>

<section class="center-content center-content--pattern">
    <div class="container">
        <div class="section-header">
            <h3 class="section-header__title"><?php esc_html_e( 'Nothing Found', 'endorphitness' ); ?></h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="center-content__text">
                    <form class="search-form__form" action="<?php echo site_url('/'); ?>" method="get">
						<input type="text" name="s" placeholder="Search For Posts.." class="search-form__field">
						<button type="submit" class="search-form__btn">Search Now<i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
					</form>
                </div>
            </div>
        </div>
    </div>
</section>
