<?php
    get_template_part('templates/blog', 'hero_module');
    get_template_part('templates/blog', 'filter_topic_module');
    get_template_part('templates/blog', 'posts_module');
    get_template_part('templates/blog', 'newsletter_module');
    get_template_part('templates/blog', 'category_module');
?>