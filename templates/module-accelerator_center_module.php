<?php
    $accelerator_center_title = get_sub_field('accelerator_center_module_title');
    $accelerator_center_button = get_sub_field('accelerator_center_module_button');
?>
<section class="accelerator-gray-block grey">
    <div class="container">
        <div class="accelerator-gray-block__column">
            <h2 class="accelerator-gray-block__title"><?php echo $accelerator_center_title; ?></h2>
            <?php if($accelerator_center_button): $target = empty($accelerator_center_button['target']) ? '_self' : '_blank'; ?>
                <a href="<?php echo $accelerator_center_button['url'] ?>" target="<?php echo $target; ?>" class="border-btn"><?php echo $accelerator_center_button['title'] ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>