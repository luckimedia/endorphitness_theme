<?php 
    $center_content_title = get_sub_field('video_module_title');
    $center_title_font_family = get_sub_field('video_module_title_font_family');
    if($center_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($center_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($center_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $center_title_font_color = get_sub_field('video_module_title_font_color');
    $center_content_title_border = get_sub_field('video_module_title_border_bottom');

    $video_module_image = get_sub_field('video_module_image');
    $video_url = get_sub_field('video_url');
    $video_block = get_sub_field('video_block');
    $video_block_count = get_sub_field('video_block_count');
    $video_left_block_title = get_sub_field('video_left_block_title');
    $video_left_block_description = get_sub_field('video_left_block_description');
    $video_block_icons = get_sub_field('video_block_icons');
?>
<section class="video-content-block">
<div class="section-header">
<h3 class="<?php if($center_title_font_color == 'green'){ echo 'section-header__title--green ';} else { echo 'section-header__title '; } ?>left-right-image-module__heading--<?php echo $font; ?><?php if($center_content_title_border == 'yes'){ echo ' section-header__title--border-bottom';} ?>"><?php echo $center_content_title; ?></h3>
</div>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="video-content-module">
                    <div class="video-content-module__image">
                        <img src="<?php echo THEME_URL; ?>/images/laptop.png" class="img-fluid video-content-module__bg-image">
                        <div class="testimonial-video" style="background-image: url(<?php echo $video_module_image; ?>);">
                            <button type="button" class="testimonial-video__btn" data-toggle="modal"
                                data-target="#glassAnimals"><img src="<?php echo THEME_URL; ?>/images/Polygon-1.png" class="img-fluid"></button>
                        </div>
                    </div>
                    <div class="video-content-module__text">
                        <h3 class="video-content-module__title"><?php echo $video_left_block_title; ?></h3>
                        <p><?php echo $video_left_block_description; ?></p>
                    </div>
                    <div class="video-content-module__icons">
                    <?php if ($video_block_icons) : foreach ($video_block_icons as $icon) :?>
                        <img src="<?php echo $icon['video_block_iocn']; ?>" class="img-fluid">
                    <?php endforeach; endif; ?>
                    </div>
                    <div class="modal fade testimonial-video-modal" id="glassAnimals" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">Polygon 1.png
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <iframe width="100%" height="315" src="<?php echo $video_url ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <!-- <video controls id="video1" style="width: 100%;">
                                        <source
                                            src="<?php// echo $video_url ?>"
                                            type="video/mp4">
                                        Your browser doesn't support HTML5 video tag.
                                    </video> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-1">
                <div class="video-content-block__column">
                <?php for($i = 0; $i < $video_block_count; $i++) { ?>
                    <div class="video-content-box">
                        <h3 class="video-content-box__title"><?php echo $video_block[$i]['video_block_title']; ?></h3>
                        <p><?php echo $video_block[$i]['video_block_description']; ?></p>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>