<?php
    $newsletter_title = get_sub_field('newsletter_title');
    $newsletter_shortcode = get_sub_field('newsletter_shortcode');
?>
<section class="newsletter-module">
    <div class="newsletter-module__row">
        <div class="newsletter-module__heading">
          <h3 class="newsletter-module__title"><?php echo $newsletter_title; ?></h3>
        </div>
        <?php echo do_shortcode($newsletter_shortcode); ?>
    </div>
</section>