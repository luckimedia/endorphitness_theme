<?php
    $newsletter_shortcode = get_field('newsletter_shortcode', 'options');
?>
<section class="newsletter-module">
    <div class="newsletter-module__row">
        <div class="newsletter-module__heading">
          <h3 class="newsletter-module__title">Subscribe to our newsletter</h3>
        </div>
        <?php echo do_shortcode($newsletter_shortcode); ?>
    </div>
</section>