<?php
  $header_logo = get_theme_mod('header_logo_one');
  $header_mobile_logo = get_theme_mod('header_logo_mobile');
  $header_login_link = get_theme_mod('header_login_url');
  $menu_locations = get_nav_menu_locations();
  $top_obj = get_term( $menu_locations['top_menu'], 'nav_menu' );
  $top_menu = wp_nav_menu([
    'menu' => $top_obj->name,
    'depth' => 2,
    'container' => false,
    'items_wrap' => '%3$s',
    'echo' => false,
    'walker' => new WP_Bootstrap_Navwalker()
  ]);
?>
<!DOCTYPE html>
<html lang="en">
  <head <?php language_attributes(); ?>>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108076703-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-108076703-1');
    gtag('config', 'AW-842765822');
    </script>
    <meta name="google-site-verification" content="4R_DVoXfM_8qIa4Nf6TsfwzerggrDbe5ptEFmFsdU44" />
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
    <!-- HEADER START -->
    <header class="header">
      <div class="header-right-patter"></div>
      <div class="container">
        <div class="header__main">
          <div class="header__top d-none d-lg-flex">
            <div class="header__logo">
              <a href="<?php echo get_site_url(); ?>"
                ><img src="<?php echo $header_logo ?>" class="img-fluid"
              /></a>
            </div>
            <div class="header__top-menu">
                <?php if( is_user_logged_in()) : 
                  $url = get_site_url().'/dashboard';
                ?>
                  <a href="<?php echo $url; ?>">Dashboard</a>
                  <a href="<?php echo get_site_url().'/cart'?>"><i class="fas fa-shopping-cart"></i></a>
                <?php else : ?>
                  <a href="<?php echo $header_login_link ?>">Login</a>
                <?php endif; ?>
            </div>
          </div>
          <div class="header__menu">
            <button
              class="hamburger d-lg-none collapsed"
              id="mobileMenu"
              type="button"
              id="nav-icon3"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </button>

            <div class="logo-mobile d-lg-none">
              <a href="<?php echo get_site_url(); ?>"
                ><img src="<?php echo $header_mobile_logo ?>" class="img-fluid"
              /></a>
            </div>

            <div class="main-navigation">
              <nav class="navbar navbar-expand-lg navbar-light">
                <div
                  class="collapse navbar-collapse"
                  id="navbarSupportedContent"
                >
                 <ul class="nav navbar-nav">
                   <?php echo $top_menu; ?>
                </ul>
               </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- HEADER END -->