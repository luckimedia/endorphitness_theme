<?php 
    $blog_category_title = get_field('blog_category_title_option', 'options');
    
    $center_title_font_family = get_field('blog_category_title_font_family_option', 'options');
    if($center_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($center_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($center_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $center_title_font_color = get_field('blog_category_title_font_color', 'options');
    $center_content_title_border = get_field('blog_category_title_border_bottom_option', 'options');

    $blog_category = get_field('blog_category_option', 'options');
    $blog_category_block_color = get_field('blog_category_block_color', 'options');
?>
<section class="blog-categories blog-categories--pattern">
    <div class="container">
        <div class="section-heading">
            <h3 class="<?php if($center_title_font_color == 'green'){ echo 'section-header__title--green ';} else { echo 'section-header__title '; } ?>left-right-image-module__heading--<?php echo $font; ?><?php if($center_content_title_border == 'yes'){ echo ' section-header__title--border-bottom';} ?>"><?php echo $blog_category_title; ?></h3>
        </div>
        <div class="blog-categories__module">
            <div class="row">
            <?php if ($blog_category) : foreach ($blog_category as $category) :

                // Get the current category image
                $catrgory_image = z_taxonomy_image_url($category->term_id);

                // Get the current category block color
                $color = get_field('category_block_color', 'category_'.$category->term_id);
                $slug =  get_site_url().'/'.$category->taxonomy.'/'.$category->slug;
            ?>
                <div class="col-lg-4">
                    <div class="blog-categories__section" style="background-color: <?php echo $color; ?>">
                        <a href="<?php echo $slug; ?>">
                            <div class="blog-categories__image" style="background-image: url(<?php echo $catrgory_image; ?>);">
                            </div>
                            <div class="blog-categories__heading">
                                <h4 class="blog-categories__title"><?php echo $category->name; ?></h4>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endforeach; endif; ?>
            </div>
        </div>
    </div>
</section>