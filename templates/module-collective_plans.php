<?php
    $collective_plan_title = get_sub_field('collective_plan_title');
    $center_title_font_family = get_sub_field('collective_plan_title_font_family');
    if($center_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($center_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($center_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $center_title_font_color = get_sub_field('collective_plan_title_font_color');
    $center_content_title_border = get_sub_field('collective_plan_title_border_bottom');

    //Pricing Module
    $pricing_boxes = get_sub_field('pricing_boxes');
    foreach($pricing_boxes as $key => $boxes){
        if($boxes['plan_type'] == 'monthly'){
            $monthly_products[] = $pricing_boxes[$key];
        }else if($boxes['plan_type'] == 'annually'){
            $annually_products[] = $pricing_boxes[$key];
        }
    }
?>
<section class="pricing-module pricing-module--pattern" id="CollectivePricingModule">
    <div class="container">
        <div class="section-header">
            <h3
                class="<?php if($center_title_font_color == 'green'){ echo 'section-header__title--green ';} else { echo 'section-header__title '; } ?>left-right-image-module__heading--<?php echo $font; ?><?php if($center_content_title_border == 'yes'){ echo ' section-header__title--border-bottom';} ?>">
                <?php echo $collective_plan_title; ?></h3>
        </div>
        <div class="pricing-module__tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#billed-monthly" role="tab">Billed
                        Monthly</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#billed-annually" role="tab">Billed Annually</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="billed-monthly" role="tabpanel">
                <div class="row justify-content-center">
                    <?php if ($monthly_products) : foreach ($monthly_products as $monthly) : ?>
                    <div class="col-lg-4">
                        <div class="pricing-block">
                            <h5 class="pricing-block__title"><?php echo $monthly['plan_name']; ?></h5>
                            <div class="pricing-block__price-box">
                                <h6 class="pricing-block__price">
                                    $<?php echo $monthly['plan_price']; ?>/mo</h6>
                                <h5 class="pricing-block__subtitle"><?php echo $monthly['plan_label']; ?></h5>
                            </div>
                            <div class="pricing-block__content">
                                <?php echo $monthly['plan_description']; ?>
                            </div>
                            <a href="<?php echo $monthly['plan_checkout_url']['url']; ?>"
                                class="btn"><?php echo $monthly['plan_checkout_url']['title']; ?></a>
                        </div>
                    </div>
                    <?php endforeach; endif; ?>
                </div>
            </div>
            <div class="tab-pane" id="billed-annually" role="tabpanel">
                <div class="row justify-content-center">
                    <?php if ($annually_products) : foreach ($annually_products as $annually) : ?>
                    <div class="col-lg-4">
                        <div class="pricing-block">
                            <h5 class="pricing-block__title"><?php echo $annually['plan_name']; ?></h5>
                            <div class="pricing-block__price-box">
                                <h6 class="pricing-block__price">
                                    $<?php echo $annually['plan_price']; ?>/year</h6>
                                <h5 class="pricing-block__subtitle"><?php echo $annually['plan_label']; ?></h5>
                            </div>
                            <div class="pricing-block__content">
                                <?php echo $annually['plan_description']; ?>
                            </div>
                            <a href="<?php echo $annually['plan_checkout_url']['url']; ?>"
                                class="btn"><?php echo $annually['plan_checkout_url']['title']; ?></a>
                        </div>
                    </div>
                    <?php endforeach; endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>