<?php 
    $hero_image = get_field('hero_image', 'options');
    $hero_title = get_field('hero_title', 'options');
    $hero_description = get_field('hero_description', 'options');
    $hero_title_tag = get_field('hero_title_tag', 'options');
?>
<section class="hero-module">
    <div class="container-fluid">
        <div class="hero-module__row">
            <div class="hero-module__image">
                <img src="<?php echo $hero_image; ?>" class="img-fluid" style="width: 100%" />
            </div>
            <div class="hero-module__content">
                <<?php echo $hero_title_tag; ?> class="hero-module__title"><?php echo $hero_title; ?></<?php echo $hero_title_tag; ?>>
                <p>
                    <?php echo $hero_description; ?>
                </p>
            </div>
        </div>
    </div>
</section>