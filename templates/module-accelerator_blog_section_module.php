<?php 
    $module_image = get_sub_field('accelerator_blog_section_module_image');
    $module_image_position = get_sub_field('accelerator_blog_section_module_image_position');
    $module_title = get_sub_field('accelerator_blog_section_module_title');
    $module_description = get_sub_field('accelerator_blog_section_module_description');
    $module_button = get_sub_field('accelerator_blog_section_module_button');
    $module_color = get_sub_field('accelerator_blog_section_color');
    if($module_image_position == 'right'){
        $right = ' accelerator-image-blog--right';
        $class = ' accelerator-image-blog__img';
    }else{
        $right = '';
        $class = '';
    }
?>
<section class="accelerator-image-blog <?php echo $right; ?>">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-7 <?php echo $class; ?>">
                <div class="accelerator-image-blog__image">
                    <img src="<?php echo $module_image; ?>" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-5">
                <div class="accelerator-image-blog__column <?php echo $module_color; ?>">
                    <h2 class="accelerator-image-blog__title"><?php echo $module_title; ?></h2>
                    <?php echo $module_description; ?>
                    <?php if($module_button): $target = empty($module_button['target']) ? '_self' : '_blank'; ?>
                        <a href="<?php echo $module_button['url'] ?>" target="<?php echo $target; ?>" class="border-btn"><?php echo $module_button['title'] ?></a>
                    <?php endif; ?>
            </div>
        </div>
    </div>
</section>