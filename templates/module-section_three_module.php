<?php 
    $section_title = get_sub_field('section_three_module_title');

    $center_title_font_family = get_sub_field('section_three_module_title_font_family');
    if($center_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($center_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($center_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $center_title_font_color = get_sub_field('section_three_module_title_font_color');
    $center_content_title_border = get_sub_field('section_three_module_title_border_bottom');

    $cards = get_sub_field('cards')
?>
<section class="blog-post-accelerator">
    <div class="container">
        <div class="section-header">
            <?php if( $section_title ): ?>
                <h3 class="<?php if($center_title_font_color == 'green'){ echo 'section-header__title--green ';} else { echo 'section-header__title '; } ?>left-right-image-module__heading--<?php echo $font; ?><?php if($center_content_title_border == 'yes'){ echo ' section-header__title--border-bottom';} ?>"><?php echo $section_title; ?></h3>
        <?php endif;?>
        </div>
        <div class="row">           
            <?php if ($cards) : foreach ($cards as $card) :?>
                <div class="col-lg-4">
                    <div class="blog-post-accelerator-col blog-post-accelerator-col--center">
                        <a href="#" class="blog-post-accelerator-col__image"
                            style="background-image: url(<?php echo $card['card_image']; ?>);">
                        </a>
                        <div class="blog-post-accelerator-col__blogs">
                            <p><?php echo $card['card_desc']; ?></p>
                            <h5 class="blog-post-accelerator-col__title "><?php echo $card['card_title']; ?></h5>
                        </div>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>