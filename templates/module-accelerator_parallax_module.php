<?php 
    $accelerator_parallax_module_image = get_sub_field('accelerator_parallax_module_image');
    $accelerator_parallax_module_title = get_sub_field('accelerator_parallax_module_title');
    $accelerator_parallax_module_content = get_sub_field('accelerator_parallax_module_content');
?>
<div class="parallax-image-module">
    <div class="parallax-image-module__img" style="background-image: url(<?php echo $accelerator_parallax_module_image; ?>);">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="parallax-image-module__section">
                        <h2 class="parallax-image-module__heading"><?php echo $accelerator_parallax_module_title; ?></h2>
                        <p><?php echo $accelerator_parallax_module_content; ?></p>
                    </div>
                </div>
                <div class="col-lg-5">
                </div>
            </div>
        </div>
    </div>
</div>