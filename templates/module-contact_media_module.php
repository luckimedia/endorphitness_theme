<?php 
    $contact_media_title = get_sub_field('contact_media_title');
    $contact_media_description = get_sub_field('contact_media_description');
    $contact_media_middle_image = get_sub_field('contact_media_middle_image');
    $contact_media_right_image1 = get_sub_field('contact_media_right_image1');
    $contact_media_right_image2 = get_sub_field('contact_media_right_image2');
?>
<section class="content-media-slide">
    <div class="container">
        <div class="content-media-slide__section">
            <div class="content-media-slide__content">
                <h5 class="content-media-slide__title"><?php echo $contact_media_title; ?></h5>
                <p><?php echo $contact_media_description; ?></p>
            </div>
            <div class="content-media-slide__media">
                <img src="<?php echo $contact_media_middle_image; ?>" class="img-fluid">
            </div>
            <div class="content-media-slide__image">
                <img src="<?php echo $contact_media_right_image1; ?>" class="img-fluid">
                <img src="<?php echo $contact_media_right_image2; ?>" class="img-fluid">
            </div> 
        </div>
    </div>
</section>