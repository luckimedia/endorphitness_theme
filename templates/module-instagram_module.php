<?php
    $instagram_shortcode = get_sub_field('instagram_plugin_shortcode');
?>
<section class="instagram-module">
    <div class="instagram-module__img">
        <?php if(!empty($instagram_shortcode)): ?>
            <?php echo do_shortcode($instagram_shortcode); ?>
        <?php else: ?>
            <img src="<?php echo THEME_URL; ?>/images/inst.png" class="img-fluid" style="width: 100%" />
        <?php endif; ?>
    </div>
</section>