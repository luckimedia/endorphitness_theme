<?php 
    $center_content_title = get_sub_field('center_content_title');
    $center_title_font_family = get_sub_field('center_content_title_font_family');
    if($center_title_font_family == 'rohja'){
        $font = 'rohja';
    }else if($center_title_font_family == 'nunito'){
        $font = 'nunito';
    }else if($center_title_font_family == 'playlist'){
        $font = 'playlist';
    }
    $center_title_font_color = get_sub_field('center_content_title_font_color');
    $center_content_title_border = get_sub_field('center_content_title_border_bottom');
    
    $center_content_description = get_sub_field('center_content_description');
    $center_content_module_column = get_sub_field('center_content_module_column');
    $center_content_module_devider_image = get_sub_field('center_content_module_devider_image');
?>
<style>
    .center-content--pattern::before {
        background-image: url(<?php echo $center_content_module_devider_image; ?>)
    }
</style>
<section class="center-content center-content--pattern">
    <div class="container">
        <?php if($center_content_title): ?>
            <div class="section-header">
                    <h3 class="<?php if($center_title_font_color == 'green'){ echo 'section-header__title--green ';} else { echo 'section-header__title '; } ?>left-right-image-module__heading--<?php echo $font; ?><?php if($center_content_title_border == 'yes'){ echo ' section-header__title--border-bottom';} ?>"><?php echo $center_content_title; ?></h3>
            </div>
        <?php endif; ?>

        <div class="row justify-content-center">
            <div class="<?php echo $center_content_module_column; ?>">
                <div class="center-content__text">
                    <p><?php echo $center_content_description; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>