<?php 
    $accelerator_blog_module_title = get_sub_field('accelerator_blog_module_title');
    $accelerator_blog_module_date = get_sub_field('accelerator_blog_module_date');
    $accelerator_blog_module_sub_title = get_sub_field('accelerator_blog_module_sub_title');
    $accelerator_blog_module_image = get_sub_field('accelerator_blog_module_image');
    $accelerator_form_short_code = get_sub_field('accelerator_form_short_code');
    $accelerator_blog_style = get_sub_field('select_style');
    if($accelerator_blog_style == 'style1'){
        $accelerator_blog_button = get_sub_field('accelerator_blog_button');
    }else{
        $accelerator_form_short_code = get_sub_field('accelerator_form_short_code');
    }
?>
<section class="accelerator-blog">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-5">
                <div class="accelerator-blog__column grey">
                    <div class="accelerator-blog__heading">
                        <h3 class="accelerator-blog__title"><?php echo $accelerator_blog_module_title; ?><span><?php echo $accelerator_blog_module_date; ?></span></h3>
                    </div>
                    <div class="accelerator-blog__form">
                        <!-- <h5 class="accelerator-blog__form-title"><?php //echo $accelerator_blog_module_sub_title; ?></h5> -->
                        <?php if($accelerator_blog_style == 'style1'): $target = empty($accelerator_blog_button['target']) ? '_self' : '_blank'; ?>
                            <a href="<?php echo $accelerator_blog_button['url'] ?>" target="<?php echo $target; ?>" class="border-btn"><?php echo $accelerator_blog_button['title'] ?></a>
                        <?php else: ?>
                            <div><?php echo do_shortcode($accelerator_form_short_code); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="accelerator-blog__image">
                    <img src="<?php echo $accelerator_blog_module_image; ?>" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>