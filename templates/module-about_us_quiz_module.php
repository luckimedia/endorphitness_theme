
<?php 
    $quiz_module_title = get_sub_field('about_us_quiz_module_title');
    $about_us_quizs = get_sub_field('about_us_quizs');
?>
<section class="about-us-quiz">
    <div class="container">
        <div class="section-header section-header--small-width">
        <?php if($quiz_module_title): ?>
            <h3 class="section-header__title"><?php echo $quiz_module_title; ?><h3>
        <?php endif; ?>
        </div>
        <div class="row justify-content-center">
        <?php if ($about_us_quizs) : $a = 1; foreach ($about_us_quizs as $quiz) :?>
            <div class="col-lg-6">
                <div class="about-us-quiz__column">
                    <h5 class="about-us-quiz__heading"><span><?php echo $a; ?>.</span><?php echo $quiz['question_title']; ?></h5>
                    <?php if($quiz['description_or_options'] == 'desc'): ?>
                        <ul class="about-us-quiz__desc">
                            <li><?php echo $quiz['question_description']; ?></li>
                        </ul>
                    <?php elseif($quiz['description_or_options'] == 'options'): ?>
                        <ul class="about-us-quiz__answer">
                            <?php $i = 'A'; if ($quiz['question_options']) : foreach ($quiz['question_options'] as $option) :?>
                                <li class="about-us-quiz__sub-answer"><span class="<?php if($option['correct_answer'] == 'true') {
                                    if($i == 'A'){echo 'bg-pattern';}
                                    else if($i == 'B'){echo 'bg-pattern-2';}
                                    else if($i == 'C'){echo 'bg-pattern-3';}
                                    else if($i == 'D'){echo 'bg-pattern-4';}
                                    else{echo 'bg-pattern';}
                                } ?>"><?php echo $i; ?>.</span><?php echo $option['option_title']; ?></li>
                            <?php $i++; endforeach; endif; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        <?php $a++; endforeach; endif; ?>
        </div>
    </div>
</section>