<?php 
    $love_hate_module_title = get_sub_field('love_hate_module_title');
    $love_hate_module_description = get_sub_field('love_hate_module_description');
    $love_hate_module_image = get_sub_field('love_hate_module_image');
?>
<section class="about-right-image-block green">
    <div class="container">
        <div class="row justify-content-center align-items-center no-gutters">
            <div class="col-lg-6 about-right-image-block__head-block">
                <div class="about-right-image-block__heading">
                    <h3 class="about-right-image-block__title"><?php echo $love_hate_module_title; ?></h3>
                </div>
                <div class="about-right-image-block__column">
                    <p><?php echo $love_hate_module_description; ?></p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-right-image-block__image">
                    <img src="<?php echo $love_hate_module_image; ?>" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>