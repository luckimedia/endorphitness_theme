<?php 
    $about_main_title = get_sub_field('about_me_module_main_title');
    $about_title = get_sub_field('about_me_module_title');
    $about_description = get_sub_field('about_me_module_description');
    $about_image = get_sub_field('about_me_module_image');
    $about_image_postition = get_sub_field('about_me_module_image_postition');
    $about_me_form_short_code = get_sub_field('about_me_form_short_code');
    if($about_image_postition == 'about-me--right'){
        $col = 'col-lg-5 about-me__col';
    }else{
        $col = 'col-lg-5';
    }
?>
<section class="about-me <?php echo $about_image_postition; ?>">
    <div class="container">
        <div class="section-header section-header--small-width">
            <?php if($about_main_title): ?>
                <h3 class="section-header__title"><?php echo $about_main_title; ?></h3>
            <?php endif; ?>
        </div>
        <div class="row no-gutters">
            <div class="<?php echo $col; ?>">
                <div class="about-me__column">
                <?php if($about_title): ?>
                    <h3 class="about-me__title"><?php echo $about_title; ?></h3>
                <?php endif; ?>
                    <p><?php echo $about_description; ?></p>
                    <div class="about-me__forms"><?php echo do_shortcode($about_me_form_short_code); ?></div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="about-me__image">
                    <img src="<?php echo $about_image; ?>" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>