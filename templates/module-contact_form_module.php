<?php 
    $contact_form_title = get_sub_field('contact_form_title');
    $contact_form_shortcode = get_sub_field('contact_form_shortcode');
?>
<section id="get-in-touch" class="contact-us">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="contact-us__form">
                    <div class="contact-us--form">
                        <h5 class="contact-us--form__title"><?php echo $contact_form_title; ?></h5>
                        <?php echo do_shortcode($contact_form_shortcode); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>