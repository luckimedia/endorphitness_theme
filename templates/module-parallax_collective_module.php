
<?php 
    $parallax_collective_background_image = get_sub_field('parallax_collective_background_image');
    $parallax_collective_module_title = get_sub_field('parallax_collective_module_title');
    $parallax_collective_module_image = get_sub_field('parallax_collective_module_image');
    $parallax_button = get_sub_field('parallax_collective_module_button');
    $parallax_collective_list = get_sub_field('parallax_collective_module_list');
?>
<div class="parallax-module">
    <div class="parallax-module__img" style="background-image: url(<?php echo $parallax_collective_background_image; ?>);">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="parallax-module__media">                        
                   <div class="iphone-model"><div class="content-box"><figure><div class="temp-wrapper"><div class="px"><div class="px__body"><div class="px__body__cut"></div><div class="px__body__speaker"></div><div class="px__body__sensor"></div><div class="px__body__mute"></div><div class="px__body__up"></div><div class="px__body__down"></div><div class="px__body__right"></div></div><div class="px__screen"><div class="px__screen__"><div class="px__screen__frame"> <img class="imessage-temp" src="<?php echo $parallax_collective_module_image; ?>" class="img-fluid" alt=""></div></div></div></div></div></figure></div></div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="parallax-module__content">
                        <h5 class="parallax-module__title"><?php echo $parallax_collective_module_title; ?></h5>
                        <ul class="parallax-module__list">
                        <?php if($parallax_collective_list) : foreach($parallax_collective_list as $list): ?>
                            <li><?php echo $list['parallax_collective_module_list_title']; ?></li>
                        <?php endforeach; endif; ?>
                        </ul>
                        <?php
                            if( $parallax_button ): 
                            $link_url = $parallax_button['url'];
                            $link_title = $parallax_button['title'];
                            $link_target = $parallax_button['target'] ? $parallax_button['target'] : '_self';
                        ?>
                            <a href="<?php echo $link_url; ?>" class="btn"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>