<?php
    get_template_part('templates/particals', 'header');
?>
<!-- Hero Module Start -->
<section class="hero-module">
    <div class="container">
        <div class="hero-module__row">
        <div class="hero-module__image">
            <img src="<?php echo get_template_directory_uri().'/' ?>images/hero.png" class="img-fluid" style="width: 100%">
        </div>
        <div class="hero-module__content">
            <h2 class="hero-module__title">
                <?php
                    /* translators: Search query. */
                    printf( __( 'Search Results for: %s', 'endorphitness' ), get_search_query());
                ?>
            </h2>
        </div>
        </div>
    </div>
</section>
<!-- Hero Module End -->

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php get_template_part('templates/content', 'search'); ?>    
<?php
endwhile;
if(function_exists('wp_paginate')):
    wp_paginate();  
endif;
else :
	get_template_part('templates/content', 'none');
endif;
    get_template_part('templates/particals', 'footer');
?>