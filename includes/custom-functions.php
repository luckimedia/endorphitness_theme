<?php

/**
 * Redirect page if user isn't logged in.
 */
function redirect_to_login($id, $logged_in) {
    $members_only = get_field('members_only', $id);
    $has_access = !$members_only || $members_only && $logged_in;
    if(!$has_access) {
        $redirect_id = get_theme_mod('redirect');
        $redirect_url = get_the_permalink($redirect_id);
        wp_redirect($redirect_url);
        exit();
    }
}

/**
 * Check Array/Key Function
 */
function array_key_check($array, $key) {
	if(is_array($array) && array_key_exists($key, $array) && '' != $array[$key]) return true;
	return false;
}

/**
 * Build link from URL
 */
function build_link_from_url($url, $children) {
    $render = '';
    $home = get_bloginfo('url');
    switch($url) :
        case '':
            $render = "<p>{$children}</p>";
            break;
        case '/' :
            $render = "<a href='{$home}'>{$children}</a>";
            break;
        default :
            $render = "<a href='{$url}'>{$children}</a>";
            break;
    endswitch;

    return $render;
}

/**
 * Get Event Icon by type
 */
function get_event_type_icon($type) {
    $icon = "";
    switch($type) :
        case "local_event":
            $icon = "<i class='fas fa-map-pin'></i>";
            break;
        case "webinar":
            $icon = "<i class='fas fa-play-circle'></i>";
            break;
        case "lunch_learn":
            $icon = "<i class='fas fa-utensils'></i>";
            break;
        case "member":
            $icon = "<i class='fas fa-lock'></i>";
            break;
        case "open":
            $icon = "<i class='fas fa-lock-open'></i>";
            break;
        default:
            break;
    endswitch;

    return $icon;
}

/**
 * Turn Query Strings into an array of values
 */
function query_string_to_array() {
    $query = [];
    parse_str($_SERVER['QUERY_STRING'], $query);
    return $query;
}

/**
 * Get all values for field from array of items (assumes ACF)
 */
function get_all_field_values($field, $items) {
    $values = [];

    foreach($items as $item) {
        $val = get_field($field, $item->ID);
        array_push($values, $val);
    }
    return array_unique($values);
}

/**
 * Get all posts that meet filters using WP_Query
 */
function get_posts_with_filters($cpt, $params=[]) {
    $event_type = isset($params['event_type']) && $params['event_type'] !== 'all' ? [
        'key' => 'event_type',
        'value' => $params['event_type'],
        'compare' => '='
    ] : '';
    $member_type = isset($params['attendees']) && $params['attendees'] !== 'all' ? [
        'key' => 'attendees',
        'value' => $params['attendees'],
        'compare' => '='
    ] : '';
    $daterange = isset($params['daterange']) && $params['daterange'] !== 'any' ? [
        'key' => 'dates_start_date',
        'value' => date('Ymd', strtotime(" + {$params['daterange']} days")),
        'compare' => '<'
    ] : "";
    $param_categories = isset($params['categories']) ? explode(',', $params['categories']) : '';

    $categories = isset($params['categories']) ? [
        'relation' => 'AND',
        [
            'taxonomy' => 'vendor-category',
            'field'    => 'slug',
            'terms'    => $param_categories,
        ],
    ] :
    '';

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $query = [
        'posts_per_page' => -1,
        'paged' => $paged,
        'post_type' => $cpt,
        'tax_query' => $categories,
        'meta_query' => [
            'relation' => 'AND',
            [
                'relation' => 'OR',
                [
                    'key' => 'dates_start_date',
                    'value' => date('Ymd', strtotime("now")),
                    'compare' => '>='
                ],
                [
                    'key' => 'dates_end_date',
                    'value' => date('Ymd', strtotime("now")),
                    'compare' => '>='
                ],
                [
                    'key' => 'dates_ongoing',
                    'value' => '0',
                    'compare' => '!='
                ]
            ],
            $event_type,
            $member_type,
            $daterange
        ]
    ];

    if ( isset($params['orderby']) ) {
        if ( $params['orderby'] !== 'title_az' && $params['orderby'] !== 'title_za' ) {
            $query['meta_key'] = $params['orderby'];
            $query['orderby'] = 'meta_value';
        } else {
            $query['meta_key'] = '';
            $query['orderby'] = 'title';
        }
        $query['order'] = $params['orderby'] === 'title_za' ? 'DESC' : 'ASC';
    }

    $results = new WP_Query($query);
    return $results;
}

/**
 * Get all posts that meet filters using WP_Query
 */
function get_vendors_with_filters( $params=[]) {
    $categories = isset($params['categories']) ? explode(',', $params['categories']) : '';

    $vendor_cat = isset($params['categories']) ? [
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'vendor-category',
                'field'    => 'slug',
                'terms'    => ['action', 'comedy'],
            ],
        ]] :
        '';

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $results = new WP_Query([
        'posts_per_page' => -1,
        'paged' => $paged,
        'post_type' => 'vendor',

    ]);
    return $results;
}

/**
 * Set up pagination for custom queries
 */
function pagination($pages = '', $range = 4) {
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}

/**
 * Set Characte Limit for content
 */
function get_excerpt($limit, $source = null){
    $excerpt = $source;
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'...';
    return $excerpt;
}

/**
 * Get social share url
 */
function get_social_share_url($site, $post) {
    $url = get_the_permalink($post->ID);
    $title = get_the_title($post->ID);
    $image = get_theme_mod('events_background_image');
    $text = get_the_excerpt($post->ID);
    $provider = get_bloginfo('title');
    $socialMedia = [
        "facebook" => "https://www.facebook.com/sharer.php?u={$url}",
        "twitter" => "https://twitter.com/intent/tweet?url={$url}&text={$title}",
        "linkedin" => "https://www.linkedin.com/shareArticle?mini=true&url={$url}&title={$title}&summary={$text}&source={$provider}",

    ];
    return $socialMedia[strtolower($site)];
}

/**
 * Adjust Hex Brightness
 */
function adjust_brightness($hex, $steps = [0, 0, 0]) {
    // Steps should be between -255 and 255. Negative = darker, positive = lighter
    $new_steps = [];
    foreach($steps as $step) {
        $new_steps[] = max(-255, min(255, $step));
    }

    // Normalize into a six character long hex string
    $hex = str_replace('#', '', $hex);
    if(strlen($hex) == 3) {
        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
    }

    // Split into three parts: R, G and B
    $color_parts = str_split($hex, 2);
    $return = '#';

    for($i = 0; $i < count($color_parts); $i++) {
        $color_parts[$i] = hexdec($color_parts[$i]); // Convert to decimal
        $color_parts[$i] = max(0,min(255,$color_parts[$i] + $new_steps[$i])); // Adjust color
        $return .= str_pad(dechex($color_parts[$i]), 2, '0', STR_PAD_LEFT); // Make two char hex code
    }

    return $return;
}

/**
 * Move Yoast to Bottom
 */
    function yoast_to_bottom() {
        return 'low';
    }
    add_filter('wpseo_metabox_prio', 'yoast_to_bottom');
add_filter( 'body_class','my_body_classes' );
function my_body_classes( $classes ) {
 
    if ( is_page('about-me') ) {

        $classes[] = 'about-us-pages';
         
    }
    if ( is_page('accelerator') ) {

        $classes[] = 'accelerator';
         
    }
    if ( is_page('online-personal-training') ) {

        $classes[] = 'training-pages';
         
    }
     
    return $classes;
     
}
