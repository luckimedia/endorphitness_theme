<?php

// Local
// $subscription_ids = array( 15267, 15270, 15273 );
// $checkout_page_id = 15254;
// $product_group_id = 15266;
// $collective_module_url = '/collective-sales#CollectivePricingModule';
// $premium_checkin_shortcode = '[contact-form-7 id="15344" title="Premium Checkin"]';
// $inner_circle_checkin_shortcode = '[contact-form-7 id="15345" title="Inner Circle Checkin"]';
// $accelerator_id = 14441;

// Live
$subscription_ids = array( 15789, 15792, 15795 );
$checkout_page_id = 15798;
$product_group_id = 15788;
$collective_module_url = '/collective#CollectivePricingModule';
$premium_checkin_shortcode = '[contact-form-7 id="15859" title="Premium Checkin"]';
$inner_circle_checkin_shortcode = '[contact-form-7 id="15860" title="Inner Circle Checkin"]';
$accelerator_id = 8820;

// Hide Subscription Category from /shop overview page
add_action( 'woocommerce_product_query', 'prefix_custom_pre_get_posts_query' );
function prefix_custom_pre_get_posts_query( $q ) {
	
	if( is_shop() || is_page('awards') ) {

	    $tax_query = (array) $q->get( 'tax_query' );
	
	    $tax_query[] = array(
	           'taxonomy' => 'product_cat',
	           'field'    => 'slug',
	           'terms'    => array( 'subscription' ),
	           'operator' => 'NOT IN'
	    );
	
	
	    $q->set( 'tax_query', $tax_query );
	}
}

// Check if user has active subscription:
function has_active_subscription($user_id = null) {
    // When a $user_id is not specified, get the current user Id
    if (null == $user_id && is_user_logged_in()) {
        $user_id = get_current_user_id();
    }

    // User not logged in we return false
    if ($user_id == 0) {
        return false;
    }

    // Get all active subscriptions for a user ID
    $active_subscriptions = get_posts(array(
        'numberposts' => 1, // Only one is enough
        'meta_key' => '_customer_user',
        'meta_value' => $user_id,
        'post_type' => 'shop_subscription', // Subscription post type
        'post_status' => 'wc-active', // Active subscription
        'fields' => 'ids', // return only IDs (instead of complete post objects)
    ));

    return sizeof($active_subscriptions) == 0 ? false : true;
}


// Get current plan's name
function get_active_plan_name($user_id = null) {
    if (null == $user_id && is_user_logged_in()) {
        $user_id = get_current_user_id();
    }
    if ($user_id == 0) {
        return false;
    }
    
    if  ( has_active_subscription() ) {
        $users_subscriptions = wcs_get_users_subscriptions($user_id);
        foreach ($users_subscriptions as $subscription){
            if ($subscription->has_status(array('active'))) {
                $items = $subscription->get_items(); 
                foreach ( $items as $item ) {
                    $product_name = $item->get_name();
                    $product_name = explode(' -', $product_name);
                    $product_name = $product_name[0];
                }
            }
        }
        return $product_name;
    }
    return false;
}

// Get current access
function plan_starter($user_id = null) {

    if (null == $user_id && is_user_logged_in()) {
        $user_id = get_current_user_id();
    }
    if ($user_id == 0) {
        return false;
    }

    $current_plan = get_active_plan_name();

    return $current_plan == 'Collective Starter' ? true : false;
}

function plan_premium($user_id = null) {

    if (null == $user_id && is_user_logged_in()) {
        $user_id = get_current_user_id();
    }
    if ($user_id == 0) {
        return false;
    }

    $current_plan = get_active_plan_name();

    return $current_plan == 'Collective Premium' ? true : false;
}

function plan_inner_circle($user_id = null) {

    if (null == $user_id && is_user_logged_in()) {
        $user_id = get_current_user_id();
    }
    if ($user_id == 0) {
        return false;
    }

    $current_plan = get_active_plan_name();

    return $current_plan == 'Collective Inner Circle' ? true : false;
}



// Redirect Active Subscribers to a different page
function wc_custom_user_redirect($redirect, $user)
{
    // Get the first of all the roles assigned to the user
    $role = $user->roles[0];
    $dashboard = admin_url();
    $user_id = $user->ID;
    $redirect_page_id = url_to_postid($redirect);
    $myaccount = get_permalink(wc_get_page_id('myaccount'));
    global $checkout_page_id;

    if( has_active_subscription($user_id) ){
        $redirect = home_url('dashboard');
    } else {
        if ($role == 'administrator') {
            //Redirect administrators to the dashboard
            $redirect = $dashboard;
        } elseif ($role == 'shop-manager') {
            //Redirect shop managers to the dashboard
            $redirect = $dashboard;
        } elseif ($role == 'editor') {
            //Redirect editors to the dashboard
            $redirect = $dashboard;
        } elseif ($role == 'author') {
            //Redirect authors to the dashboard
            $redirect = $dashboard;
        } elseif ($role == 'customer') {
            //Redirect customers and subscribers to the "My Account" page
            // Replace with custom checkout page ID
            if ($redirect_page_id == $checkout_page_id) {
                $current_url = home_url(add_query_arg(array($_GET), $wp->request));
                $url_array = parse_url($current_url);
                $path = $url_array['path'].'?'.$url_array['query'];
                $key = 'handle';
                $filteredURL = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $path);
                $redirect = home_url('checkout-subscription/'.$filteredURL.'handle=step_two');
            } else {
                $redirect = $myaccount;
            }
        } elseif ($role == 'subscriber') {
            $redirect = home_url('dashboard');
        } else {
            //Redirect any other role to the previous visited page or, if not available, to the home
            $redirect = wp_get_referer() ? wp_get_referer() : home_url();
        }
    }
    return $redirect;
}
add_filter('woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2);

// Add Body Class for active subscriber
function urban_square_bodyclass_names($classes)
{
    // if we are not in a mobile environment
    if (has_active_subscription() || current_user_can('manage_options')) {
        // add 'desktop' to the $classes array
        $classes[] = 'active-subscriber-or-super-admin';
        return $classes;
    } else {
        return $classes;
    }
}

add_filter('body_class', 'urban_square_bodyclass_names');


// Check if subscription product is being purchased from somewhere else and remove it from the cart
add_action( 'woocommerce_check_cart_items', 'conditionally_remove_specific_cart_item' );
function conditionally_remove_specific_cart_item() {
    global $subscription_ids;
    $items_to_remove = $subscription_ids; // Product Ids to be removed from cart
    $cart_page_id = get_option( 'woocommerce_cart_page_id' ); 
    $page_id = get_queried_object_id();


    if ($page_id == $cart_page_id) {
        // Loop through cart items
        foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            if( array_intersect( $items_to_remove, array( $cart_item['variation_id'], $cart_item['product_id'] ) ) ) {
                // Remove cart item
                WC()->cart->remove_cart_item( $cart_item_key );
            }
        }
    }
}

// Checking if the subscription product is in the cart
function woo_is_in_cart($product_id) {
    global $woocommerce;
    foreach ($woocommerce->cart->get_cart() as $key => $val) {
        $_product = $val['data'];
        if ($product_id == $_product->get_id()) {
            return true;
        }
    }
    return false;
}

add_action( 'template_redirect', 'wc_custom_redirect_after_purchase' );
function wc_custom_redirect_after_purchase() {
    if ( ! is_wc_endpoint_url( 'order-received' ) ) return;

    // Define the product IDs in this array
    global $subscription_ids;
    $product_ids = $subscription_ids; // or an empty array if not used
    $redirection = false;

    global $wp;
    $order_id =  intval( str_replace( 'checkout/order-received/', '', $wp->request ) ); // Order ID
    $order = wc_get_order( $order_id ); // Get an instance of the WC_Order Object

    // Iterating through order items and finding targeted products
    foreach( $order->get_items() as $item ){
        if( in_array( $item->get_product_id(), $product_ids ) ) {
            $redirection = true;
            break;
        }
    }
    
    // Make the custom redirection when a targeted product has been found in the order
    if( $redirection ){
        wp_redirect(home_url('dashboard'));
        exit;
    }
}

// Redirect the product group
add_action('template_redirect','redirect_product_group');
function redirect_product_group() {
    global $product_group_id;
    global $collective_module_url;
    if ( is_single($product_group_id) && is_product() ) {
        wp_redirect($collective_module_url);
        exit;
    }
}

// Hide register form on my-account page
add_action('wp_head', 'custom_my_account_css');
function custom_my_account_css(){
    echo '<style>
    body.woocommerce-account .u-column2 { display: none; }
    @media (min-width: 992px) { body.woocommerce-account .u-column1 {flex: 0 0 100%; max-width: 100%;}}
    body div.nsl-container .nsl-button-google[data-skin=light] {
        background-color: #db4a3c !important;
        border-color: #db4a3c;
        color: #fff;
        border-radius: 5px;
      }
      
      @media (max-width: 768px) {
        body div.nsl-container-block .nsl-container-buttons a {
          max-width: 100%;
        }
      }
      @media (min-width: 768px) {
        .body .auth-wrapper {
          display: flex;
          max-width: 510px;
          justify-content: space-between;
        }
      }
      .nsl-button.nsl-button-default.nsl-button-facebook {
        box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.25);
      }
      
      .auth-wrapper {
        margin-left: auto;
        margin-right: auto;
      }
      @media (min-width: 768px) {
        .auth-wrapper {
          display: flex;
          max-width: 530px;
          justify-content: space-between;
          margin-bottom: 1rem;
        }
      }
    </style>';

    echo '<script>
    jQuery(document).ready(function () {
        jQuery(".no_subscriptions > a").attr("href", "/collective#CollectivePricingModule")
    });
    </script>';
}


// Updates made April 4 2020

add_filter('gettext', 'change_specific_add_to_cart_notice', 10, 3);
add_filter('ngettext', 'change_specific_add_to_cart_notice', 10, 3);
function change_specific_add_to_cart_notice($translated, $text, $domain)
{
    if ($text === 'You cannot add another "%s" to your cart.' && $domain === 'woocommerce' && !is_admin()) {
        // Replacement text (where "%s" is the dynamic product name)
        $translated = __('<span class="remove-me">You cannot add another "%s" to your cart.</span>', $domain);
    }
    return $translated;
}

// Custom Registration Form

function wooc_extra_register_fields()
{?>
<!-- First Name -->
<p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e('First name', 'woocommerce');?><span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if (!empty($_POST['billing_first_name'])) {
    esc_attr_e($_POST['billing_first_name']);
}
    ?>" />
</p>
<!-- Last Name -->
<p class="form-row form-row-last">
    <label for="reg_billing_last_name"><?php _e('Last name', 'woocommerce');?><span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if (!empty($_POST['billing_last_name'])) {
        esc_attr_e($_POST['billing_last_name']);
    }
    ?>" />
</p>
<div class="clear"></div>
<?php
}
add_action('woocommerce_register_form_start', 'wooc_extra_register_fields');

/**

 * register fields Validating.

 */

function wooc_validate_extra_register_fields($username, $email, $validation_errors)
{

    if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {

        $validation_errors->add('billing_first_name_error', __('First name is required.', 'woocommerce'));

    }

    if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {

        $validation_errors->add('billing_last_name_error', __('Last name is required.', 'woocommerce'));

    }

    return $validation_errors;
}

add_action('woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3);

/**
 * Below code save extra fields.
 */
function wooc_save_extra_register_fields($customer_id)
{
    if (isset($_POST['billing_first_name'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']));
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
    }
    if (isset($_POST['billing_last_name'])) {
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']));
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
    }

}
add_action('woocommerce_created_customer', 'wooc_save_extra_register_fields');

function iconic_remove_password_strength()
{
    wp_dequeue_script('wc-password-strength-meter');
}
add_action('wp_print_scripts', 'iconic_remove_password_strength', 10);

// Query Parameter for custom checkout
function add_query_vars_filter($vars)
{
    $vars[] = "handle";
    return $vars;
}

add_filter('query_vars', 'add_query_vars_filter');


// DRIP Integration
function woosubscription_hold($subscription) {
    $order_billing_email = $subscription->get_billing_email(); 
    $items = $subscription->get_items(); 
    foreach ( $items as $item ) {
        $product_name = $item->get_name();
        $product_name = explode(' -', $product_name);
        $product_name = $product_name[0];
    }
    // var_dump($product_name);
    // exit;
    $User = get_user_by( 'email', $order_billing_email );
    $current_user_email = $User->user_email;
    $drip_apiKey = 'MzI0NzA3MDA2NjgxZDViOTI3ODYxZDFlOWRlMjU4NDg=';
    // Request One
    $endpointURL = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Member';
    $ch = curl_init($endpointURL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result = curl_exec($ch);
    curl_close($ch);

    // Request Two
    if ($product_name) {
        $endpointURLDP = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/'.$product_name;
        $chDP = curl_init($endpointURLDP);
        curl_setopt($chDP, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($chDP, CURLOPT_HTTPHEADER, 
            array(
                'Content-Type: application/json',
                'User-Agent: Endorphitness (endorphitness.com)',
                'Authorization: Basic '.$drip_apiKey
            )
        );
        $resultDP = curl_exec($chDP);
        curl_close($chDP);
    }
    

    // Request Three
    $endpointURL2 = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+%28Free+Trial%29';
    $ch2 = curl_init($endpointURL2);
    curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch2, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result2 = curl_exec($ch2);
    curl_close($ch2);
    
    // Request Four
    $endpointURL3 = 'https://api.getdrip.com/v2/6795546/tags/';
    $ch3 = curl_init($endpointURL3);
    $data =  [
        "tags" => [
                [
                    "email" => $current_user_email, 
                    "tag" => "Collective Hold" 
                ] 
            ] 
        ]; 
    $jsonData = json_encode($data);
    curl_setopt($ch3, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch3, CURLOPT_POST, 1);
    curl_setopt($ch3, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch3, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result3 = curl_exec($ch3);
    curl_close($ch3);
    
}

function woosubscription_active($subscription) {
    $order_billing_email = $subscription->get_billing_email(); 
    $items = $subscription->get_items(); 
    foreach ( $items as $item ) {
        $product_name = $item->get_name();
        $product_name = explode(' -', $product_name);
        $product_name = $product_name[0];
    }
    $User = get_user_by( 'email', $order_billing_email );
    $current_user_email = $User->user_email;
    $drip_apiKey = 'MzI0NzA3MDA2NjgxZDViOTI3ODYxZDFlOWRlMjU4NDg=';

    // Request One
    $endpointURL = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Hold';
    $ch = curl_init($endpointURL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result = curl_exec($ch);
    curl_close($ch);

    // Request Two
    $endpointURL2 = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Cancel';
    $ch2 = curl_init($endpointURL2);
    curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch2, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result2 = curl_exec($ch2);
    curl_close($ch2);

    // Request Four
    if ($product_name) {
        $endpointURLDP = 'https://api.getdrip.com/v2/6795546/tags/';
        $chDP = curl_init($endpointURLDP);
        $data =  [
            "tags" => [
                    [
                        "email" => $current_user_email, 
                        "tag" => $product_name
                    ] 
                ] 
            ]; 
        $jsonData = json_encode($data);
        curl_setopt($chDP, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($chDP, CURLOPT_POST, 1);
        curl_setopt($chDP, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($chDP, CURLOPT_HTTPHEADER, 
            array(
                'Content-Type: application/json',
                'User-Agent: Endorphitness (endorphitness.com)',
                'Authorization: Basic '.$drip_apiKey
            )
        );
        $resultDP = curl_exec($chDP);
        curl_close($chDP);
    }
    
}

function woosubscription_switched($subscription) {
    $order_billing_email = $subscription->get_billing_email(); 
    $items = $subscription->get_items(); 
    foreach ( $items as $item ) {
        $product_name = $item->get_name();
        $product_name = explode(' -', $product_name);
        $product_name = $product_name[0];
    }
    // var_dump($items);
    // exit;
    $User = get_user_by( 'email', $order_billing_email );
    $current_user_email = $User->user_email;
    $drip_apiKey = 'MzI0NzA3MDA2NjgxZDViOTI3ODYxZDFlOWRlMjU4NDg=';

    // Request One
    $endpointURL = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Hold';
    $ch = curl_init($endpointURL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result = curl_exec($ch);
    curl_close($ch);

    // Request Two
    $endpointURL2 = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Cancel';
    $ch2 = curl_init($endpointURL2);
    curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch2, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result2 = curl_exec($ch2);
    curl_close($ch2);

    // Request Three
    $endpointURL3 = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Starter';
    $ch3 = curl_init($endpointURL3);
    curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch3, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result3 = curl_exec($ch3);
    curl_close($ch3);

    // Request Four
    $endpointURL4 = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Premium';
    $ch4 = curl_init($endpointURL4);
    curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch4, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result4 = curl_exec($ch4);
    curl_close($ch4);

    // Request Five
    $endpointURL5 = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Inner+Circle';
    $ch5 = curl_init($endpointURL5);
    curl_setopt($ch5, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch5, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result5 = curl_exec($ch5);
    curl_close($ch5);

    // Request Four
    if ($product_name) {
        $endpointURLDP = 'https://api.getdrip.com/v2/6795546/tags/';
        $chDP = curl_init($endpointURLDP);
        $data =  [
            "tags" => [
                    [
                        "email" => $current_user_email, 
                        "tag" => $product_name
                    ] 
                ] 
            ]; 
        $jsonData = json_encode($data);
        curl_setopt($chDP, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($chDP, CURLOPT_POST, 1);
        curl_setopt($chDP, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($chDP, CURLOPT_HTTPHEADER, 
            array(
                'Content-Type: application/json',
                'User-Agent: Endorphitness (endorphitness.com)',
                'Authorization: Basic '.$drip_apiKey
            )
        );
        $resultDP = curl_exec($chDP);
        curl_close($chDP);
    }
    
}


function woosubscription_cancel($subscription) {
    $order_billing_email = $subscription->get_billing_email(); 
    $items = $subscription->get_items(); 
    foreach ( $items as $item ) {
        $product_name = $item->get_name();
        $product_name = explode(' -', $product_name);
        $product_name = $product_name[0];
    }
    $User = get_user_by( 'email', $order_billing_email );
    $current_user_email = $User->user_email;
    $drip_apiKey = 'MzI0NzA3MDA2NjgxZDViOTI3ODYxZDFlOWRlMjU4NDg=';
    // Request One
    $endpointURL = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+Member';
    $ch = curl_init($endpointURL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result = curl_exec($ch);
    curl_close($ch);

    // Request Two
    if ($product_name) {
        $endpointURLDP = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/'.$product_name;
        $chDP = curl_init($endpointURLDP);
        curl_setopt($chDP, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($chDP, CURLOPT_HTTPHEADER, 
            array(
                'Content-Type: application/json',
                'User-Agent: Endorphitness (endorphitness.com)',
                'Authorization: Basic '.$drip_apiKey
            )
        );
        $resultDP = curl_exec($chDP);
        curl_close($chDP);
    }

    // Request Three
    $endpointURL2 = 'https://api.getdrip.com/v2/6795546/subscribers/'.$current_user_email.'/tags/Collective+%28Free+Trial%29';
    $ch2 = curl_init($endpointURL2);
    curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch2, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result2 = curl_exec($ch2);
    curl_close($ch2);
    
    // Request Four
    $endpointURL3 = 'https://api.getdrip.com/v2/6795546/tags/';
    $ch3 = curl_init($endpointURL3);
    $data =  [
        "tags" => [
                [
                    "email" => $current_user_email, 
                    "tag" => "Collective Cancel" 
                ] 
            ] 
        ]; 
    $jsonData = json_encode($data);
    curl_setopt($ch3, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch3, CURLOPT_POST, 1);
    curl_setopt($ch3, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch3, CURLOPT_HTTPHEADER, 
        array(
            'Content-Type: application/json',
            'User-Agent: Endorphitness (endorphitness.com)',
            'Authorization: Basic '.$drip_apiKey
        )
    );
    $result3 = curl_exec($ch3);
    curl_close($ch3);
    if( is_user_logged_in() && !is_admin() ){
        wp_redirect(home_url('subscription-cancelled'));
        exit;
    }
}

// Accelerator Product
add_action( 'woocommerce_thankyou', 'bbloomer_check_order_product_id' );
    function bbloomer_check_order_product_id( $order_id ){
    global $accelerator_id;
    $order = wc_get_order( $order_id );
    $drip_apiKey = 'MzI0NzA3MDA2NjgxZDViOTI3ODYxZDFlOWRlMjU4NDg=';
    $items = $order->get_items(); 
    $billing_email = $order->get_billing_email();
    foreach ( $items as $item_id => $item ) {
    $product_id = $item->get_variation_id() ? $item->get_variation_id() : $item->get_product_id();
    if ( $product_id === $accelerator_id ) {
            $endpointURL3 = 'https://api.getdrip.com/v2/6795546/tags/';
            $ch3 = curl_init($endpointURL3);
            $data =  [
                "tags" => [
                        [
                            "email" => $billing_email, 
                            "tag" => "Accelerator Member" 
                        ] 
                    ] 
                ]; 
            $jsonData = json_encode($data);
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch3, CURLOPT_POST, 1);
            curl_setopt($ch3, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch3, CURLOPT_HTTPHEADER, 
                array(
                    'Content-Type: application/json',
                    'User-Agent: Endorphitness (endorphitness.com)',
                    'Authorization: Basic '.$drip_apiKey
                )
            );
            $result3 = curl_exec($ch3);
            curl_close($ch3);
        }
    }
}



add_action( 'woocommerce_subscription_status_on-hold', 'woosubscription_hold');
add_action( 'woocommerce_subscription_status_active', 'woosubscription_active');
add_action( 'woocommerce_subscription_status_cancelled', 'woosubscription_cancel');
add_action( 'woocommerce_scheduled_subscription_expiration', 'woosubscription_cancel');
add_action( 'woocommerce_subscription_status_pending-cancel', 'woosubscription_cancel');
add_action( 'woocommerce_subscriptions_switch_completed', 'woosubscription_switched');