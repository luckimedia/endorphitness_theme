<?php

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support('title-tag');

/*
 * Enable support for Post Thumbnails on posts and pages.
 *
 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
 */
add_theme_support('post-thumbnails');

/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support(
	'html5',
	array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		'style',
		'script',
	)
);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function endorphitness_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'endorphitness' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'endorphitness' ),
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		),
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Media Sidebar', 'endorphitness' ),
			'id'            => 'media-sidebar',
			'description'   => esc_html__( 'Add widgets here.', 'endorphitness' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h4 class="media-content-menu__title">',
			'after_title'   => '</h4>',
		),
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Widget Area 1', 'endorphitness' ),
			'id'            => 'sidebar-2',
			'description'   => esc_html__( 'Add widgets here.', 'endorphitness' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="footer__title">',
			'after_title'   => '</h5>',
		),
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Widget Area 2', 'endorphitness' ),
			'id'            => 'sidebar-3',
			'description'   => esc_html__( 'Add widgets here.', 'endorphitness' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="footer__title">',
			'after_title'   => '</h5>',
		),
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Widget Area 3', 'endorphitness' ),
			'id'            => 'sidebar-4',
			'description'   => esc_html__( 'Add widgets here.', 'endorphitness' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="footer__title">',
			'after_title'   => '</h5>',
		),
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Widget Area 4', 'endorphitness' ),
			'id'            => 'sidebar-5',
			'description'   => esc_html__( 'Add widgets here.', 'endorphitness' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="footer__title">',
			'after_title'   => '</h5>',
		),
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Widget Area 5', 'endorphitness' ),
			'id'            => 'sidebar-6',
			'description'   => esc_html__( 'Add widgets here.', 'endorphitness' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="footer__title">',
			'after_title'   => '</h5>',
		),
	);
}
add_action( 'widgets_init', 'endorphitness_widgets_init' );

/**
 * Disable Gutenberg Blocks. (OMG!)
 */
global $wp_version;
if(version_compare($wp_version, '5', '>=')) {
	add_filter('use_block_editor_for_post', '__return_false', 10);
	add_action('wp_enqueue_scripts', function() {
		wp_dequeue_style('wp-block-library');
	}, 100);
}

// Speed up ACF
add_filter('acf/settings/remove_wp_meta_box', '__return_true');

//Disable Editor for page
add_action( 'init', function() {
    remove_post_type_support( 'page', 'editor' );
}, 99);

//Add woocommerce support to your theme (Visible to product detail page)
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

//Limit Text
function get_first_paragraph()
{
    global $post;
    $str = wpautop(get_the_content());
    $str = substr($str, 0, strpos($str, '</p>') + 4);
    $str = strip_tags($str, '<a><strong><em>');
    return $str;
}

function limit_text($text, $limit){
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

add_filter('woocommerce_form_field_args',  'wc_form_field_args',10,3);
function wc_form_field_args($args, $key, $value) {
  $args['input_class'] = array( 'form-control' );
  return $args;
} 

add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );
function add_loginout_link( $items, $args ) {
    if (is_user_logged_in() && $args->menu == 'Main-menu') {
        $items .= '<li class="d-block d-lg-none menu-item menu-item-type-post_type menu-item-object-page dropdown pull-left"><a href="'.site_url('dashboard').'">Dashboard</a></li>';
    }
    elseif (!is_user_logged_in() && $args->menu == 'Main-menu') {
        $items .= '<li class="d-block d-lg-none menu-item menu-item-type-post_type menu-item-object-page dropdown pull-left"><a href="'.site_url('my-account').'">Login</a></li>';
    }
    return $items;
}

// Adding Gated Dashboard's url to WooCommerce Menu
add_filter('woocommerce_account_menu_items', 'misha_one_more_link');
function misha_one_more_link($menu_links)
{
    if (current_user_can('manage_options')) {
        $new = array('anyuniquetext123' => 'Gym Dashboard');
        $menu_links = array_slice($menu_links, 0, 1, true)
         + $new
         + array_slice($menu_links, 1, null, true);
    }
    return $menu_links;
}

add_filter('woocommerce_get_endpoint_url', 'misha_hook_endpoint', 10, 4);
function misha_hook_endpoint($url, $endpoint, $value, $permalink)
{
    if ($endpoint === 'anyuniquetext123') {
        // ok, here is the place for your custom URL, it could be external
        $url = home_url('dashboard');
    }
    return $url;
}

// Changing Original Dashboard to Gated Dashboard in WooCommerce Menu
add_filter('woocommerce_account_menu_items', 'misha_rename_downloads');
function misha_rename_downloads($menu_links)
{
    // $menu_links['TAB ID HERE'] = 'NEW TAB NAME HERE';
    $menu_links['dashboard'] = 'My Account';
    return $menu_links;
}

//Remove Wordpress body classes on search page
add_filter('body_class','alter_search_classes');
function alter_search_classes($classes) {
    if(is_search()){
       return array();
    } else {
       return $classes;
    }
}
