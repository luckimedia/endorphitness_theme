<?php
    /**
     * Add options page to Wordpress with ACF
     */

	// Blog Page
	if( function_exists('acf_add_options_page') ) {
	    acf_add_options_page(array(
	        'page_title'    => 'Blog Page',
	        'menu_title'    => 'Blog Page',
	        'menu_slug'     => 'theme-general-settings',
	        'capability'    => 'edit_posts',
	        'redirect'      => false
	    ));
	}

	// Gym Dashboard Options Page
	if (function_exists('acf_add_options_page')) {
		acf_add_options_page('Gym Dashboard Option');
	}
?>