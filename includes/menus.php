<?php

/**
 * Register menus for the theme
 */
register_nav_menus([
    'top_menu' => esc_html__('Top Nav', 'endorphitness'),
    'programs' => esc_html__('Programs','endorphitness'),
    'popular' => esc_html__('Popular', 'endorphitness'),
    'farmacy' => esc_html__('Farmacy', 'endorphitness'),
    'company' => esc_html__('Company', 'endorphitness'),
    'contact' => esc_html__('Contact', 'endorphitness'),
    'sidenav' => esc_html__('SideMenu', 'endorphitness')
]);
