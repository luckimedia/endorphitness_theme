<?php

/**
 * Pest Control Enqueue scripts and styles.
 */
function endorphitness_enqueue_scripts() {
    //CSS PART
    wp_enqueue_style('fonts','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css', array(), 1, 'all');
    wp_enqueue_style('slick', THEME_URL . '/css/slick.css', array(), 1, 'all');
    wp_enqueue_style('slick-theme', THEME_URL . '/css/slick-theme.css', array(), 1, 'all');
    wp_enqueue_style('fonts-css', THEME_URL . '/fonts/fonts.css', array(), 1, 'all');
    wp_enqueue_style('main', THEME_URL . '/css/style.css', array(), 1, 'all');

    //JS PART
    wp_enqueue_script('jqueryjs', THEME_URL . '/js/jquery-3.4.1.min.js', array(), 1, 1, 1);
    wp_enqueue_script('popper-js', THEME_URL . '/js/popper.min.js', array(), 1, 1, 1);
    wp_enqueue_script('bootstrap', THEME_URL . '/js/bootstrap.min.js', array(), 1, 1, 1);
    wp_enqueue_script('slick-min-js', THEME_URL . '/js/slick.min.js', array(), 1, 1, 1);
    wp_enqueue_script('main-js', THEME_URL . '/js/main.js', array(), 1, 1, 1);
}
add_action( 'wp_enqueue_scripts', 'endorphitness_enqueue_scripts' );

?>