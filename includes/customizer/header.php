<?php
// Add Header Section
$wp_customize->add_section( 'header' , array(
    'title'      => 'Header',
    'priority'   => 30,
) );

// Add Header Settings
$wp_customize->add_setting('header_logo_one', array(
    'default' => '',
    'type' => 'theme_mod',
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo_one', array(
    'label' => __( 'Header Logo', 'endorphitness' ),
    'section' => 'header',
    'settings' => 'header_logo_one',
    ))
);

$wp_customize->add_setting('header_logo_mobile', array(
    'default' => '',
    'type' => 'theme_mod',
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo_mobile', array(
    'label' => __( 'Header Mobile Logo', 'endorphitness' ),
    'section' => 'header',
    'settings' => 'header_logo_mobile',
    ))
);

$wp_customize->add_setting('header_login_url', array(
    'default' => '',
    'type' => 'theme_mod',
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control( 'header_login_url', array(
    'label' => __( 'Header Login URL' ),
    'type' => 'text',
    'section' => 'header',
    'settings' => 'header_login_url'
) );
