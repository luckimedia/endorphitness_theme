<?php

// Add Footer Section
$wp_customize->add_section( 'footer' , array(
    'title'      => 'Footer',
    'priority'   => 30,
));

// Add Footer Settings
$wp_customize->add_setting('footer_logo_one', array(
    'default' => '',
    'type' => 'theme_mod',
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo_one', array(
    'label' => __( 'Footer Logo', 'endorphitness' ),
    'section' => 'footer',
    'settings' => 'footer_logo_one',
    ))
);

$wp_customize->add_setting('footer_image', array(
    'default' => '',
    'type' => 'theme_mod',
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_image', array(
    'label' => __( 'Footer Image', 'endorphitness' ),
    'section' => 'footer',
    'settings' => 'footer_image',
    ))
);

