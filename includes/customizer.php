<?php

/**
 * Add our Customizer content
 */
function endorphitness_customize_register( $wp_customize ) {
    include_once(__DIR__ . '/customizer/header.php');
    include_once(__DIR__ . '/customizer/footer.php');
}
add_action( 'customize_register', 'endorphitness_customize_register' );