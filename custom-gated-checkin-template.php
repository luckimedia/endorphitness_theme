<?php
/**
 * Template Name: Gated Checkin Template
 */
wc_clear_notices();
?>
<!-- App Header -->
<?php include('subscription-src/includes/global-app-header.php'); ?>
<!-- End App header -->

<?php 
if (plan_starter()) {
    echo("<script>location.href = '".home_url()."/dashboard';</script>");
}    
?>

<main class="dashboard-main">
    <section class="section intro">
        <?php include('subscription-src/includes/global-app-header-partial.php'); ?>

        <!-- Middle Section -->
        <div class="panels-parent">
            <div class="container">
                <div class="content-box">
                    <h1 class="text-green h2" style="font-weight: 900;">Awesome, <span
                            class="text-purple"><?php echo $current_user->user_firstname; ?>!</span> Let's check in with
                        your health.</h1>
                    <p>Please fill out the form below so that we can learn more.</p>
                </div>
            </div>

            <div class="container mt-4">
                <div class="ui-form-wrapper long-content-form">
                    <?php if (plan_inner_circle()) {?>
                    <?php echo do_shortcode($inner_circle_checkin_shortcode); ?>
                    <script>
                    document.addEventListener('wpcf7mailsent', function(event) {
                        gtag('event', 'click', {
                            event_category: 'form_submission',
                            event_action: 'inner_circle_checkin_form'
                        });
                    }, false);
                    </script>
                    <?php } else { ?>
                    <?php echo do_shortcode($premium_checkin_shortcode); ?>
                    <script>
                    document.addEventListener('wpcf7mailsent', function(event) {
                        gtag('event', 'click', {
                            event_category: 'form_submission',
                            event_action: 'premium_checkin_form'
                        });
                    }, false);
                    </script>
                    <?php } ?>
                </div>
            </div>

        </div>
        <!-- End Middle Section -->

        <div class="app-footer hide-below-1200">
            <div class="container text-center">
                <h6>Copyright <?php echo date('Y'); ?></h6>
            </div>
        </div>
    </section>
</main>

<!-- App Footer -->
<?php include('subscription-src/includes/global-app-footer.php'); ?>
<!-- End App Footer -->