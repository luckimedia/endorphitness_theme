<?php
/**
 * Template Name: Single Recipes
 *  Template Post Type: recipes
 */
?>
<!-- App Header -->
<?php include('subscription-src/includes/global-app-header.php'); ?>
<!-- End App header -->
<main class="dashboard-main workouts-overview workouts-single">
    <section class="section intro">
        <?php include('subscription-src/includes/global-app-header-partial.php'); ?>

        <!-- Middle Section -->
        <div class="panels-parent">
            <div class="container">
                <?php 
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post(); ?>
                <div class="single-wrapper">
                    <a class="hide-below-1200" href="<?php echo home_url('recipes'); ?>">
                        <div class="back-arrow">
                            <i class="fas fa-arrow-circle-left"></i>
                        </div>
                    </a>
                    <div class="xmlsma--sx">
                        <div class="skuq-ml-j0xb title-wrapper text-center">
                            <h1 class="text-green"><?php the_title(); ?></h1>
                            <!--                                                 <p class="text-green date"><?php echo get_the_date(); ?></p> -->
                        </div>
                        <?php if (has_post_thumbnail( $post->ID ) ): ?>
                        <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                        <div class="skuq-ml-j0xb featured-image-wrapper">
                            <figure>
                                <img src="<?php echo $image[0]; ?>" alt="" />
                            </figure>
                        </div>
                        <?php endif; ?>
                        <div class="skuq-ml-j0xb content-wrapper">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?php } // end while
                    } // end if
                ?>
            </div>
        </div>
        <!-- End Middle Section -->

        <div class="app-footer hide-below-1200">
            <div class="container text-center">
                <h6>Copyright <?php echo date('Y'); ?></h6>
            </div>
        </div>
    </section>
</main>

<!-- App Footer -->
<?php include('subscription-src/includes/global-app-footer.php'); ?>
<!-- End App Footer -->