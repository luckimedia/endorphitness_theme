<?php
if(class_exists('acf')) {
    $options_pages = [
        '/modules-all.php' => true
    ];

    foreach($options_pages as $option => $load) {
        if($load) {
            include __DIR__ . $option;
        }
    }
}