<?php
/**
 * Template Name: Single Stretching
 *  Template Post Type: stretching
 */
?>
<!-- App Header -->
<?php include('subscription-src/includes/global-app-header.php'); ?>
<!-- End App header -->

<main class="dashboard-main workouts-overview workouts-single">
    <section class="section intro">
        <?php include('subscription-src/includes/global-app-header-partial.php'); ?>

        <!-- Middle Section -->
        <div class="panels-parent">
            <div class="container">
                <?php 
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post(); ?>
                <div class="single-wrapper">
                    <a class="hide-below-1200" href="<?php echo home_url('stretching-mobility'); ?>">
                        <div class="back-arrow">
                            <i class="fas fa-arrow-circle-left"></i>
                        </div>
                    </a>
                    <div class="xmlsma--sx">
                        <div class="skuq-ml-j0xb title-wrapper text-left pb-3 pb-lg-0">
                            <h1 class="text-purple font-fancy mt-3"><?php the_title(); ?></h1>
                        </div>
                        <?php if( have_rows('flexibility-video') ):
                                    while ( have_rows('flexibility-video') ) : the_row(); ?>
                        <div class="video-library">
                            <div class="row align-items-center">
                                <div class="col-md-5">
                                    <div class="video-wrapper js-modal-btn"
                                        data-video-id="<?php the_sub_field('video-id'); ?>">
                                        <figure>
                                            <img src="<?php the_sub_field('thumbnail'); ?>" alt="Video Thumbnail" />
                                            <div class="fader"></div>
                                            <i class="fab fa-youtube"></i>
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="content-wrapper">
                                        <h2><?php the_sub_field('title'); ?></h2>
                                        <p><?php the_sub_field('description'); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>

                        <div class="skuq-ml-j0xb content-wrapper ml-0">
                            <hr class="bg-purple" />
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <?php } // end while
                    } // end if
                ?>
            </div>
        </div>
        <!-- End Middle Section -->

        <div class="app-footer hide-below-1200">
            <div class="container text-center">
                <h6>Copyright <?php echo date('Y'); ?></h6>
            </div>
        </div>
    </section>
</main>

<!-- App Footer -->
<?php include('subscription-src/includes/global-app-footer.php'); ?>
<!-- End App Footer -->

<script src="<?php echo get_template_directory_uri(); ?>/subscription-src/video-modal.js"></script>
<script>
jQuery(document).ready(function() {
    jQuery(".js-modal-btn").modalVideo();
});
</script>