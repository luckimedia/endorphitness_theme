<?php

get_template_part('templates/particals', 'header');
?>
<!-- Hero Module Start -->
  <section class="hero-module">
    <div class="container">
      <div class="hero-module__row">
        <div class="hero-module__image">
          <img src="<?php ECHO THEME_URL; ?>/images/hero.png" class="img-fluid">
        </div>
        <div class="hero-module__content">
          <h2 class="hero-module__title">404 ! Not Found</h2>
        </div>
      </div>
    </div>
  </section>
  <!-- Hero Module End -->

  <!-- Center Content Start -->
  <section class="center-content center-content--patterns">
        <div class="container">
            <div class="section-header section-header--small-width">
                <h2 class="section-header__title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'pest-control' ); ?></h2>
            </div>
        </div>
    </section>
    <!-- Center Content End -->

<?php
get_template_part('templates/particals', 'footer');