<?php
    /**
     * Add options page to Wordpress with ACF
     */

	if( function_exists('acf_add_options_page') ) {
	    acf_add_options_page(array(
	        'page_title'    => 'Theme Setting',
	        'menu_title'    => 'Theme Setting',
	        'menu_slug'     => 'theme-general-settings',
	        'capability'    => 'edit_posts',
	        'redirect'      => false
	    ));

		acf_add_options_sub_page(array(
			'page_title'    => 'Blog Page',
			'menu_title'    => 'Blog Page',
			'parent_slug'   => 'theme-general-settings',
		));
	}
?>